(defsystem :tamias
  :author "Brandon Blundell | Neon Frost"
  :maintainer "Brandon Blundell | Neon Frost"
  :license "MIT"
  :version "0.9.57"
  :description "A game engine built and designed in Common LISP."
  :default-component-class cl-source-file.cl
  :build-operation program-op
  :build-pathname "VIDYA"
  :entry-point "cl-user::main"
  ;;  :depends-on (:tamias-sdl2)
  :components ((:module "CORE"
		:serial t
		:components
		((:module "engine"
		  :serial t
		  :components
		  ((:module "init"
		    :serial t
		    :components
			    ((:file "documentor")
			     (:file "tamias")
			     (:file "console")
			     (:file "aux-lib-pkg")
			     (:file "aux-lib")
			     (:file "timer")
			     (:file "gui.init")
			     (:file "main")
			     (:file "strings")
			     (:file "colors")
			     (:file "keyboard")
			     (:file "states")
			     (:file "state-input")
			     (:file "state-keyboard")
			     (:file "state-mouse")
			     (:file "mouse")
			     (:file "gfx-base")
			     (:file "tamias-imports")))
;;		   (:file "macros" :type "cl")
		   (:module "logic"
		    :serial t
		    :components
			    ((:module "core"
			      :serial t
			      :components
				      ((:file "entity-lib")
			     ;;			     (:file "entity-generics")
			     ;;			     (:file "entity")
				       (:file "vectors")
				       (:file "math")
		     		       (:file "bounding-box")
				       (:file "particles" :type "lisp")))
			     (:module "scripting"
			      :serial t
			      :components
				      ((:file "define-script")
				       (:file "macro-symbols")))
			     #|(:modlue "Physics"
			     :serial t
			     :components|#))
		   (:file "text-handler")
		   (:file "logic-loops")
		   (:file "render-loops")
		   (:file "init-assets")
		   ))))))
  
