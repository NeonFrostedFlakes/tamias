(defun tam-reduce (x by)
  "Divide x by by until x can no longer be reduced, and return remainder."
  (if (> x by)
      (* (- x
	    (* by (floor (/ x by))))
	 1.0)
      x)
  )

(defun deg->rad (d)
  (/ (* d pi) 180))

(defun rad->deg (r)
  (/ (* r 180) pi))

(defmacro make-mat3 ()
  `(make-array '(3 3) :initial-contents '((1.0 0.0 0.0)
					  (0.0 1.0 0.0)
					  (0.0 0.0 1.0))))
(defmacro make-mat4 ()
  `(make-array '(4 4) :initial-contents '((1.0 0.0 0.0 0.0)
					  (0.0 1.0 0.0 0.0)
					  (0.0 0.0 1.0 0.0)
					  (0.0 0.0 0.0 1.0))))

(defun mat3->4 (matA)
  (let ((ret-mat (make-mat4)))
    (loop for i below 3
       do (loop for j below 3
	     do (setf (aref ret-mat i j) (aref matA i j))))
    ret-mat))

(defmacro mat3->mat4 (matA)
  `(mat3->4 ,matA))
		       
(defun mat2-det (mat-A)
  (- (* (aref mat-A 0 0)
	(aref mat-A 1 1))
     (* (aref mat-A 0 1)
	(aref mat-A 1 0))))

(defun mat3-det (mat-A)
  (let ((det 0.0))
    (incf det (* (aref mat-A 0 0)
		 (- (* (aref mat-A 1 1)
		       (aref mat-A 2 2))
		    (* (aref mat-A 1 2)
		       (aref mat-A 2 1)))))
    (incf det (* -1
		 (aref mat-A 0 1)
		 (- (* (aref mat-A 1 0)
		       (aref mat-A 2 2))
		    (* (aref mat-A 1 2)
		       (aref mat-A 2 0)))))
    (incf det (* (aref mat-A 0 2)
		 (- (* (aref mat-A 1 0)
		       (aref mat-A 2 1))
		    (* (aref mat-A 1 1)
		       (aref mat-A 2 0)))))
    det))

(defun mat4-transpose (mat-A)
  (let ((ret-mat (make-mat4)))
    (dotimes (i 4)
      (dotimes (j 4)
	(setf (aref ret-mat j i) (aref mat-A i j))))
    ret-mat))

(defun mat4-adj (mat-A)
  (let ((trans-mat (mat4-transpose mat-A))
	(ret-mat (make-mat4)))
    (dotimes (ir 4)
      (dotimes (jr 4)
	(let ((3x3-mat (make-array '(3 3))))
	  (let ((i3 0)
		(j3 0))
	    (dotimes (i 4)
	      (dotimes (j 4)
	      	(if (and (not (eq i ir))
			 (not (eq j jr)))
		    (progn (setf (aref 3x3-mat i3 j3) (aref trans-mat i j))
			   (incf i3)))
		(if (eq i3 3)
		    (setf i3 0
			  j3 (1+ j3)))
		)))
	  (let ((det (mat3-det 3x3-mat)))
	    (if (oddp (+ ir jr))
		(setf det (* det -1)))
	    ;;	    (setf det (* (expt -1 (+ ir jr 2)) det))
	    #|	    (if (and (evenp ir)
	    (oddp jr))
	    (setf det (* det -1)))
	    (if (and (oddp ir)
	    (evenp jr))
	    (setf det (* det -1)))|#
	    (setf (aref ret-mat ir jr) det)))))
    ret-mat))

(defun mat4-det (mat-A)
  (let ((det 0.0)
	(mat-B (make-array '(3 3) :initial-contents (list (list (aref mat-A 1 1) (aref mat-A 1 2) (aref mat-A 1 3))
							  (list (aref mat-A 2 1) (aref mat-A 2 2) (aref mat-A 2 3))
							  (list (aref mat-A 3 1) (aref mat-A 3 2) (aref mat-A 3 3)))))
	(mat-C (make-array '(3 3) :initial-contents (list (list (aref mat-A 0 1) (aref mat-A 0 2) (aref mat-A 0 3))
							  (list (aref mat-A 2 1) (aref mat-A 2 2) (aref mat-A 2 3))
							  (list (aref mat-A 3 1) (aref mat-A 3 2) (aref mat-A 3 3)))))
	(mat-D (make-array '(3 3) :initial-contents (list (list (aref mat-A 0 1) (aref mat-A 0 2) (aref mat-A 0 3))
							  (list (aref mat-A 1 1) (aref mat-A 1 2) (aref mat-A 1 3))
							  (list (aref mat-A 3 1) (aref mat-A 3 2) (aref mat-A 3 3)))))
	(mat-E (make-array '(3 3) :initial-contents (list (list (aref mat-A 0 1) (aref mat-A 0 2) (aref mat-A 0 3))
							  (list (aref mat-A 1 1) (aref mat-A 1 2) (aref mat-A 1 3))
							  (list (aref mat-A 2 1) (aref mat-A 2 2) (aref mat-A 2 3))))))
    (setf det (+ (* (aref mat-A 0 0) (mat3-det mat-B))
		 (* -1 (aref mat-A 1 0) (mat3-det mat-C))
		 (* (aref mat-A 2 0) (mat3-det mat-D))
		 (* -1 (aref mat-A 3 0) (mat3-det mat-E))))
    det))


(defun mat4-inverse (mat-A)
  (let ((ret-mat (mat4-adj mat-A))
	(det (mat4-det mat-A)))
    (dotimes (i 4)
      (dotimes (j 4)
	(setf (aref ret-mat i j) (/ (aref ret-mat i j) det))))
    ret-mat))

#|
;;Code transferred from: https://stackoverflow.com/questions/2624422/efficient-4x4-matrix-inverse-affine-transform
;;                       Answered by "Robin Hilliard" (as in "   var s0 : Number = ...")
(defun mat4-inverse (matA)
(let* ((ret-mat (make-mat4))
(s0 (- (* (aref matA 0 0) (aref matA 1 1)) (* (aref matA 1 0) (aref matA 0 1))))
(s1 (- (* (aref matA 0 0) (aref matA 1 2)) (* (aref matA 1 0) (aref matA 0 2))))
(s2 (- (* (aref matA 0 0) (aref matA 1 3)) (* (aref matA 1 0) (aref matA 0 3))))
(s3 (- (* (aref matA 0 1) (aref matA 1 2)) (* (aref matA 1 1) (aref matA 0 2))))
(s4 (- (* (aref matA 0 1) (aref matA 1 3)) (* (aref matA 1 1) (aref matA 0 3))))
(s5 (- (* (aref matA 0 2) (aref matA 1 3)) (* (aref matA 1 2) (aref matA 0 3))))

(c5 (- (* (aref matA 2 2) (aref matA 3 3)) (* (aref matA 3 2) (aref matA 2 3))))
(c4 (- (* (aref matA 2 1) (aref matA 3 3)) (* (aref matA 3 1) (aref matA 2 3))))
(c3 (- (* (aref matA 2 1) (aref matA 3 2)) (* (aref matA 3 1) (aref matA 2 2))))
(c2 (- (* (aref matA 2 0) (aref matA 3 3)) (* (aref matA 3 0) (aref matA 2 3))))
(c1 (- (* (aref matA 2 0) (aref matA 3 2)) (* (aref matA 3 0) (aref matA 2 2))))
(c0 (- (* (aref matA 2 0) (aref matA 3 1)) (* (aref matA 3 0) (aref matA 2 1))))
(invdet 0))
(setf invdet (/ 1 (- (* s0 c5) (+ (* s1 c4) (+ (* s2 c3) (- (* s3 c2) (+ (* s4 c1) (* s5 c0)))))))

(aref ret-mat 0 0) (* (+ (- (* (aref matA 1 1) c5) (* (aref matA 1 2) c4)) (* (aref matA 1 3) c3)) invdet)
(aref ret-mat 0 1) (* (- (+ (* (aref matA 0 1) c5 -1)  (* (aref matA 0 2) c4)) (* (aref matA 0 3) c3)) invdet)
(aref ret-mat 0 2) (* (+ (- (* (aref matA 3 1) s5) (* (aref matA 3 2) s4)) (* (aref matA 3 3) s3)) invdet)
(aref ret-mat 0 3) (* (- (+ (* (aref matA 2 1) s5 -1) (* (aref matA 2 2) s4)) (* (aref matA 2 3) s3)) invdet)

(aref ret-mat 1 0) (* (- (+ (* (aref matA 1 0) c5 -1) (* (aref matA 1 2) c2)) (* (aref matA 1 3) c1)) invdet)
(aref ret-mat 1 1) (* (+ (- (* (aref matA 0 0) c5 ) (* (aref matA 0 2) c2)) (* (aref matA 0 3) c1)) invdet)
(aref ret-mat 1 2) (* (- (+ (* (aref matA 3 0) s5 -1) (* (aref matA 3 2) s2)) (* (aref matA 3 3) s1)) invdet)
(aref ret-mat 1 3) (* (+ (- (* (aref matA 2 0) s5) (* (aref matA 2 2) s2)) (* (aref matA 2 3) s1)) invdet)

(aref ret-mat 2 0) (* (+ (- (* (aref matA 1 0) c4) (* (aref matA 1 1) c2)) (* (aref matA 1 3) c0)) invdet)
(aref ret-mat 2 1) (* (- (+ (* (aref matA 0 0) c4 -1) (* (aref matA 0 1) c2)) (* (aref matA 0 3) c0)) invdet)
(aref ret-mat 2 2) (* (+ (- (* (aref matA 3 0) s4) (* (aref matA 3 1) s2)) (* (aref matA 3 3) s0)) invdet)
(aref ret-mat 2 3) (* (- (+ (* (aref matA 2 0) s4 -1) (* (aref matA 2 1) s2)) (* (aref matA 2 3) s0)) invdet)

(aref ret-mat 3 0) (* (- (+ (* (aref matA 1 0) c3 -1) (* (aref matA 1 1) c1)) (* (aref matA 1 2) c0)) invdet)
(aref ret-mat 3 1) (* (+ (- (* (aref matA 0 0) c3) (* (aref matA 0 1) c1)) (* (aref matA 0 2) c0)) invdet)
(aref ret-mat 3 2) (* (- (+ (* (aref matA 3 0) s3 -1) (* (aref matA 3 1) s1)) (* (aref matA 3 2) s0)) invdet)
(aref ret-mat 3 3) (* (+ (- (* (aref matA 2 0) s3) (* (aref matA 2 1) s1)) (* (aref matA 2 2) s0)) invdet))
ret-mat))
|#

(defun mat4-scalar (mat-A scalar)
  (let ((ret-mat (make-mat4)))
    (loop for i below 4
	  do (loop for j below 4
		   do (setf (aref ret-mat i j) (* (aref mat-A i j) scalar))))
    ret-mat))

(defun mat4*mat4 (mat-A mat-B)
  (let ((res-mat (make-mat4))
	(res 0))
    (dotimes (i 4)
      (dotimes (j 4)
	(loop for k below 4
	      do (incf res (* (aref mat-A i k)
			      (aref mat-B k j)))
		 ;;	     (format t "MATA: ~D ~D: ~D ~%MATB: ~D ~D ~D~%" i k (aref mat-A i k) k j (aref mat-B k j))
	      )
	(setf (aref res-mat i j) res
	      res 0)))
    res-mat))

(defun mat3*mat3 (mat-A mat-B)
  (let ((res-mat (make-mat3))
	(res 0))
    (dotimes (i 3)
      (dotimes (j 3)
	(loop for k below 3
	      do (incf res (* (aref mat-A i k)
			      (aref mat-B k j)))
		 ;;	     (format t "MATA: ~D ~D: ~D ~%MATB: ~D ~D ~D~%" i k (aref mat-A i k) k j (aref mat-B k j))
	      )
	(setf (aref res-mat i j) res
	      res 0)))
    res-mat))


(defun mat4+mat4 (mat-A mat-B)
  (let ((res-mat (make-mat4)))
    (dotimes (i 4)
      (dotimes (j 4)
	(setf (aref res-mat i j) (+ (aref mat-A i j) (aref mat-B i j)))))
    res-mat))

(defun mat4-mat4 (mat-A mat-B)
  (let ((res-mat (make-mat4)))
    (dotimes (i 4)
      (dotimes (j 4)
	(setf (aref res-mat i j) (- (aref mat-A i j) (aref mat-B i j)))))
    res-mat))

(defun mat3*vec3 (mat-A vecA)
  (let ((ret-vec (vector 0 0 0))
	(ind-sum 0.0))
    (loop for i below 3
       do (loop for j below 3
	     do (incf ind-sum
		      (* (aref mat-A i j) (aref vecA j))))
	 (setf (aref ret-vec i) ind-sum
	       ind-sum .0))
    ret-vec))


(defun mat4*vec4 (mat-A vecA)
  (let ((vec4? t)
	(res nil)
	(ind-res 0.0))
    (if (eq (car (array-dimensions vecA)) 3)
	(setf res (make-array '(4) :initial-contents (list (aref vecA 0) (aref vecA 1) (aref vecA 2) 1.0))
	      vec4? nil)
	(setf res vecA)) ;;original located in ALT
    (loop for i below 4
	  do (loop for j below 4
		   do (if (not vec4?)
			  (if (eq j 3)
			      (setf ind-res (+ ind-res (aref mat-A i j)))
			      (setf ind-res (+ ind-res (* (aref mat-A i j) (aref vecA j)))))
			  (setf ind-res (+ ind-res (* (aref mat-A i j) (aref vecA j))))))
	     (setf (aref res i) ind-res
		   ind-res 0.0))
    (if vec4?
	res
	(make-array '(3) :initial-contents (list (aref res 0) (aref res 1) (aref res 2))))))

(defmacro mat4*vec3 (mat-A vecA)
  `(mat4*vec4 ,mat-A ,vecA))

(defun mat4*vec4-alt (mat-A vecA)
  (let ((vec4? t)
	(res nil)
	(ind-res 0.0))
    (if (eq (car (array-dimensions vecA)) 3)
	(setf res (make-array '(4) :initial-contents (list (aref vecA 0) (aref vecA 1) (aref vecA 2) 1.0))
	      vec4? nil)
	(setf res (make-array '(4) :initial-contents (list (aref vecA 0) (aref vecA 1) (aref vecA 2) (aref vecA 3)))))
    (loop for i below 4
	  do (loop for j below 4
		   do (if (not vec4?)
			  (if (eq j 3)
			      (setf ind-res (+ ind-res (aref mat-A j i)))
			      (setf ind-res (+ ind-res (* (aref mat-A j i) (aref vecA j)))))
			  (setf ind-res (+ ind-res (* (aref mat-A j i) (aref vecA j))))))
	     (setf (aref res i) ind-res
		   ind-res 0.0))
    (if vec4?
	res
	(make-array '(3) :initial-contents (list (aref res 0) (aref res 1) (aref res 2))))))

#|
(defun vec4-scalar (vecA scalar)
)
|#

(defmacro vec3? (vecA)
  `(if (eq (car (type-of ,vecA)) 'simple-vector)
       (eq (length ,vecA) 3)))
  

(defmacro vec3! (&optional (x 0) (y 0) (z 0))
  `(vector ,x ,y ,z))

(defmacro vec2->vec3 (vec2)
  `(vec3! (aref ,vec2 0) (aref ,vec2 1)))


(defun vec3*vec3 (vecA vecb)
  (let ((ret-vec (vector 0 0 0)))
    (loop for i below 3
       do (setf (aref ret-vec i)
		(* (aref vecA i) (aref vecb i))))
    ret-vec))


(defun vec3-scalar (vecA scalar)
  (let ((res-vec (vector (aref vecA 0) (aref vecA 1) (aref vecA 2))))
    (loop for i below 3
	  do (setf (aref res-vec i) (* (aref vecA i) scalar)))
    res-vec))

(defun vec3+vec3 (vecA vecB)
  (let ((ret-vec #(0 0 0)))
    (dotimes (i 3)
      (setf (aref ret-vec i) (+ (aref vecA i) (aref vecB i))))
    ret-vec))

(defun vec3-vec3 (vecA vecB)
  (let ((ret-vec #(0 0 0)))
    (dotimes (i 3)
      (setf (aref ret-vec i) (- (aref vecA i) (aref vecB i))))
    ret-vec))

(defun vec3-length (vec &optional x y z)
  (let ((x (or x (elt vec 0)))
	(y (or y (elt vec 1)))
	(z (or z (elt vec 2))))
    (sqrt (+ (* x x) (* y y) (* z z)))))

(defun vec3-normalize (vec &optional x y z)
  (let ((v-len (vec3-length vec x y z)))
    (vector (/ (or x (elt vec 0)) v-len)
	    (/ (or y (elt vec 1)) v-len)
	    (/ (or z (elt vec 2)) v-len))))

(defun vec3-cross (vecA vecB)
  (flet ((va (idx) (aref vecA idx))
	 (vb (idx) (aref vecB idx)))
    (vector (- (* (va 1) (vb 2)) (* (va 2) (vb 1)))
	    (- (- (* (va 0) (vb 2)) (* (va 2) (vb 0))))
	    (- (* (va 0) (vb 1)) (* (va 1) (vb 0))))))


