(defun 2d-arr (rows columns &rest values)
  (let ((n 0)
	(ret-mat (make-array (list rows columns))))
    (if values
	(loop for i below rows
	   do (let ((val (elt values n)))
		(if (listp val)
		    (progn (loop for j below columns
			      do (setf (aref ret-mat i j) (elt val j)))
			   (incf n))
		    (loop for j below columns
		       do (setf (aref ret-mat i j) (elt values n))
			 (incf n))))))
    ret-mat))

(defun x-axle (angle)
  (let ((angle (deg->rad angle)))
    (2d-arr 3 3
	    1.0 .0 .0
	    .0 (cos angle) (- (sin angle))
	    .0 (sin angle) (cos angle))))

(defun y-axle (angle)
  (let ((angle (deg->rad angle)))
    (2d-arr 3 3
	    (cos angle) .0 (- (sin angle))
	    .0 1.0 .0
	    (sin angle) .0 (cos angle))))

(defun z-axle (angle)
  (let ((angle (deg->rad angle)))
    (2d-arr 3 3
	    (cos angle) (- (sin angle)) 0
	    (sin angle) (cos angle) 0
	    0 0 1.0)))

(defun axle-rotation (angle &optional (axle #(0.0 .0 .0)))
  (if (symbolp axle)
      (case axle
	(x (x-axle angle))
	(y (y-axle angle))
	(z (z-axle angle)))
      (if (not (eq (car (type-of axle)) 'simple-vector))
	  (error "Axle supplied to axle-rotation not a vector!")
	  (cond ((> (elt axle 0) 0)
		 (x-axle angle))
		((> (elt axle 1) 0)
		 (y-axle angle))
		((> (elt axle 2) 0)
		 (z-axle angle))))))
		
  
  

(defun calculate-perspective-matrix (&key (asp (/ 900 700)) (near 1.0) (far 300.0) (fov 80) (right 240) (bottom 240))
"Assuming that Nika is 3.5 units away from camera, she should fill up 3/4 of the vertical screen space at 60 units high."
  (let* ((scale (tan (/ (* pi (deg->rad (* .5 fov))) 180)))
	 (bottom (- bottom (- scale)))
	 (top (- bottom))
	 (right (+ right (* asp scale)))
	 (l (- right)))
;;    (gl:frustum -1.0 1.0 (- asp) asp (+ 1.0 world-scale) 10000.0)
;;              b    t      l     r          n              f
    (setf perspective-matrix (make-mat4)
	  (aref perspective-matrix 0 0) (/ (* 2.0 near) (- right l))
	  (aref perspective-matrix 0 2) (* 1.0 (/ (+ right l) (- right l)))

	  (aref perspective-matrix 2 2) (/ (- (+ far near)) (- far near))
	  (aref perspective-matrix 3 2) (/ (* -2.0 far near) (- far near))

	  (aref perspective-matrix 1 1) (/ (* 2.0 near) (- top bottom))
	  (aref perspective-matrix 1 2) (* 1.0 (/ (+ top bottom) (- top bottom)))

#|Y-up
	  (aref perspective-matrix 2 2) (* -1.0 (/ (+ far near) (- far near)))
	  (aref perspective-matrix 2 3) (* 1.0 (/ (* -2 far near) (- far near)))

	  (aref perspective-matrix 1 1) (* 1.0 (/ (* 2 near) (- top bottom)))
	  (aref perspective-matrix 1 2) (* 1.0 (/ (+ top bottom) (- top bottom)))
|#	  
	  (aref perspective-matrix 3 3) 1.0
	  (aref perspective-matrix 2 3) -1)))

(defmacro c-p-m ()
  (calculate-perspective-matrix))

(c-p-m)

(defvar y-rot-acc 1)

 (defun update-camera-pos ()
  (let ((c-r-x (aref (camera-rot t-camera) 0))
	(c-r-y (aref (camera-rot t-camera) 1)))
    (if (> c-r-x 90)
	(setf (aref (camera-rot t-camera) 0) 90)
	(if (< c-r-x -90)
	    (setf (aref (camera-rot t-camera) 0) -90)))
    (if (> c-r-y 360)
	(setf (aref (camera-rot t-camera) 1) 0
	      y-rot-acc 1)
	(if (< c-r-y 0)
	    (setf (aref (camera-rot t-camera) 1) 360))))
  
#|  (dotimes (n 2)
    (if (> (aref (camera-rot t-camera) (1+ n)) 360)
	(setf (aref (camera-rot t-camera) (1+ n)) 360)
	(if (< (aref (camera-rot t-camera) (1+ n)) 0)
	    (setf (aref (camera-rot t-camera) (1+ n)) 0))))|#
  #|  (let ((direction (vector (* (sin (deg->rad (aref (camera-rot t-camera) 1))) (cos (deg->rad (aref (camera-rot t-camera) 0))))
			   (sin (deg->rad (aref (camera-rot t-camera) 0)))
			   (* (- (cos (deg->rad (aref (camera-rot t-camera) 1)))) (cos (deg->rad (aref (camera-rot t-camera) 0))))))
  (look-at nil))|#
  (let* ((camera-rot (camera-rot t-camera))
	 (direction (mat3*vec3 (mat3*mat3 (y-axle (aref camera-rot 1))
					  (x-axle (aref camera-rot 0))) #(0 0 -3))))
    (setf (camera-front t-camera) direction)))

(defun look-at-mat ()
  (update-camera-pos)
  (let ((up #(0.0 1.0 .0))
	(camR nil)
	(camU nil)
	(direction (vector (* (sin (deg->rad (aref (camera-rot t-camera) 1))) (cos (deg->rad (aref (camera-rot t-camera) 0))))
			   (sin (deg->rad (aref (camera-rot t-camera) 0)))
			   (* (- (cos (deg->rad (aref (camera-rot t-camera) 1)))) (cos (deg->rad (aref (camera-rot t-camera) 0))))))
	(look-at nil)
	(camD nil)
	(camera (camera-pos t-camera))
	(ret-mat (make-mat4)))
    (setf look-at (vec3-normalize direction)
	  (camera-front t-camera) look-at
	  camD (vec3-normalize (vec3-vec3 camera (vec3+vec3 camera look-at)))
	  camR (vec3-normalize (vec3-cross camD up))
	  camU (vec3-cross camD camR))
    (loop for i below 3
       do (setf (aref ret-mat 0 i) (aref camR i)
		(aref ret-mat 1 i) (aref camU i) 
		(aref ret-mat 2 i) (aref camD i)))
    (let ((p-mat (make-mat4)))
      (setf (aref p-mat 0 3) (- (aref camera 0))
	    (aref p-mat 1 3) (- (aref camera 1))
	    (aref p-mat 2 3) (- (aref camera 2)))	     
      (mat4*mat4 p-mat (mat4-transpose ret-mat)))))


(defun alt-look-at (pos targ)
  (let ((up #(.0 .0 1.0))
	(camR nil)
	(camU nil)
	(camD nil)
	(ret-mat (make-mat4)))
    (setf camD (vec3-normalize (vec3-vec3 pos targ))
	  camR (vec3-normalize (vec3-cross up camD))
	  camU (vec3-cross camD camR))
    (loop for i below 3
       do (setf (aref ret-mat 0 i) (aref camR i)
		(aref ret-mat 1 i) (aref camU i) 
		(aref ret-mat 2 i) (aref camD i)))
    (let ((p-mat (make-mat4)))
      (setf (aref p-mat 0 3) (- (aref world-loc 0))
	    (aref p-mat 1 3) (- (aref world-loc 1))
	    (aref p-mat 2 3) (- (aref world-loc 2)))	     
      (mat4*mat4 p-mat ret-mat))))
    

"EYE AFFECTS ROTATION
CENTER AFFECTS"

(defun calc-cam-mat ()
  ;;
  ;;world-rotation, camera, 
  ;;
  (let ((transform-mat (make-mat4))
	(rot-mat-x (make-mat4))
	(rot-mat-y (make-mat4))
	(rot-mat-z (make-mat4))
	(trans-mat (make-mat4))
	)
    (setf (aref rot-mat-x 1 1) (cos (deg->rad (aref world-rotation 0)))
	  (aref rot-mat-x 1 2) (- (sin (deg->rad (aref world-rotation 0))))
	  (aref rot-mat-x 2 1) (sin (deg->rad (aref world-rotation 0)))
	  (aref rot-mat-x 2 2) (cos (deg->rad (aref world-rotation 0)))
	  
	  (aref rot-mat-y 0 0) (cos (deg->rad (aref world-rotation 1)))
	  (aref rot-mat-y 0 2) (sin (deg->rad (aref world-rotation 1)))
	  (aref rot-mat-y 2 0) (- (sin (deg->rad (aref world-rotation 1))))
	  (aref rot-mat-y 2 2) (cos (deg->rad (aref world-rotation 1)))
	  
	  (aref rot-mat-z 0 0) (cos (deg->rad (aref world-rotation 2)))
	  (aref rot-mat-z 0 1) (- (sin (deg->rad (aref world-rotation 2))))
	  (aref rot-mat-z 1 0) (sin (deg->rad (aref world-rotation 2)))
	  (aref rot-mat-z 1 1) (cos (deg->rad (aref world-rotation 2)))
	  
	  (aref trans-mat 3 0) (- (aref camera 0))
	  (aref trans-mat 3 1) (- (aref camera 1))
	  (aref trans-mat 3 2) (- (aref camera 2))
	  transform-mat (mat4*mat4 (mat4*mat4 (mat4-inverse rot-mat-z) (mat4*mat4 (mat4-inverse rot-mat-y) (mat4-inverse rot-mat-x))) (mat4-inverse trans-mat)))
    (print transform-mat))  
    )

(defun calculate-view-matrix ()
  )

(defun calculate-orthogonal-matrix (&key (asp (/ 900 700)) (near 1.0) (far 300.0) (right 700) (bottom 700))
  (let* ((n near)
	 (f far)
	 (b bottom)
	 (top (- b))
	 (r (* right asp))
	 (l (- r)))
;;    (gl:frustum -1.0 1.0 (- asp) asp (+ 1.0 world-scale) 10000.0)
;;              b    t      l     r          n              f
    (setf perspective-matrix (make-mat4)
	  (aref perspective-matrix 0 0) (/ 2.0 (- r l))
	  (aref perspective-matrix 1 1) (/ (* 2.0 n) (- top b))
	  (aref perspective-matrix 2 2) (/ -2 (+ f n) (- f n))
	  (aref perspective-matrix 3 0) (- (/ (+ r l) (- r l)))
	  (aref perspective-matrix 3 1) (- (/ (+ b top) (- top b)))
	  (aref perspective-matrix 3 2) (- (/ (+ f n) (- f n)))
	  (aref perspective-matrix 3 3) 1.0)))

(defmacro c-o-m (&key (asp (/ 900 700)) (near 1.0) (far 300.0) (right 700) (bottom 700))
  (calculate-orthogonal-matrix :asp asp :near near :far far :right right :bottom bottom))

(c-o-m)

(setf perspective-matrix (2d-arr 4 4
				 '(4 0.0 0.0 0.0)
				 '(0.0 2 0.0 0.0)
				 '(0.0 0.0 -2.2222468e-8 0.0)
				 '(.01 .01 -1.006689 1.0)))
"This matrix may not be accurate"


#|Y-up
	  (aref perspective-matrix 2 2) (* -1.0 (/ (+ far near) (- far near)))
	  (aref perspective-matrix 2 3) (* 1.0 (/ (* -2 far near) (- far near)))

	  (aref perspective-matrix 1 1) (* 1.0 (/ (* 2 near) (- top bottom)))
	  (aref perspective-matrix 1 2) (* 1.0 (/ (+ top bottom) (- top bottom)))
|#	  



#|
(defun calculate-perspective-matrix ()
  (let* ((asp (/ 300 400.0)) ;;asp = width/height
	 (near .1)
	 (scale (* (tan (deg->rad 45.0)) near))
	 (b (- scale))
	 (b_t scale)
	 (r (* asp scale))
	 (l (- r))
	 (far 1000.0))
;;    (gl:frustum -1.0 1.0 (- asp) asp (+ 1.0 world-scale) 10000.0)
;;              b    t      l     r          n              f
		(setf (aref perspective-matrix 0 0) (* 1.0 (/ (* 2 near) (- r l)))
		      (aref perspective-matrix 1 1) (* 1.0 (/ (* 2 near) (- b_t b)))
		      (aref perspective-matrix 2 2) (* -1.0 (/ (+ far near) (- far near)))
		      (aref perspective-matrix 3 3) 0
		      
		      (aref perspective-matrix 0 2) (* 1.0 (/ (+ r l) (- r l)))
		      
		      (aref perspective-matrix 1 2) (* 1.0 (/ (+ b_t b) (- b_t b)))
		      (aref perspective-matrix 2 3) (* 1.0 (/ (* -2 far near) (- far near)))
		      (aref perspective-matrix 3 2) -1)))

(defun calculate-perspective-matrix ()
  (let* ((asp (/ 300 400.0)) ;;asp = width/height
	 (near .001)
	 (scale (* (tan (deg->rad 45.0)) near))
	 (bottom (- scale))
	 (top scale)
	 (r (* asp scale))
	 (l (- r))
	 (far 1000.0))
;;    (gl:frustum -1.0 1.0 (- asp) asp (+ 1.0 world-scale) 10000.0)
    ;;              b    t      l     r          n              f
    (setf perspective-matrix (make-mat4)
	  (aref perspective-matrix 0 0) (* 1.0 (/ (* 2 near) (- r l)))
	  
	  (aref perspective-matrix 2 1) (* 1.0 (/ (* 2 near) (- top bottom)))

	  (aref perspective-matrix 1 0) (* 1.0 (/ (+ r l) (- r l)))
	  (aref perspective-matrix 1 1) (* 1.0 (/ (+ top bottom) (- top bottom)))
	  (aref perspective-matrix 1 2) (* -1.0 (/ (+ far near) (- far near)))
	  (aref perspective-matrix 1 3) -1
	  

	  (aref perspective-matrix 3 2) (* 1.0 (/ (* -2 far near) (- far near)))
	  (aref perspective-matrix 3 3) 0)
	  perspective-matrix))
;;	  perspective-matrix (mat4-transpose perspective-matrix))))
	  
|#


