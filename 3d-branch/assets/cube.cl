(defvar cube (let* ((vertices (list (make-vertex :values (make-array '(3) :initial-contents '(1.0 1.0 1.0)));;front face
				    (make-vertex :values (make-array '(3) :initial-contents '(1.0 0.0 1.0))) ;;1
				    (make-vertex :values (make-array '(3) :initial-contents '(0.0 0.0 1.0))) ;;2
				    (make-vertex :values (make-array '(3) :initial-contents '(0.0 1.0 1.0)))
				    ;;right face
				    (make-vertex :values (make-array '(3) :initial-contents '(1.0 1.0 0.0)))
				    (make-vertex :values (make-array '(3) :initial-contents '(1.0 0.0 0.0))) ;;5
				    ;;left face
				    (make-vertex :values (make-array '(3) :initial-contents '(0.0 1.0 0.0)))
				    (make-vertex :values (make-array '(3) :initial-contents '(0.0 0.0 0.0))))) ;;7
		    (polygons nil))
		      (loop for abc in '((0 1 2) (2 3 0) ;;front
					 (0 4 5) (5 1 0) ;;right
					 (0 3 4) (3 6 4) ;;top
					 (4 5 7) (7 6 4) ;;back
					 (6 3 7) (3 2 7) ;;left
					 (1 5 2) (5 7 2));;bottom
			 do (let ((polygon (make-polygon)))
			      (setf (point-position-index (polygon-a polygon)) (car abc)
				    (point-position-index (polygon-b polygon)) (cadr abc)
				    (point-position-index (polygon-c polygon)) (caddr abc))
			      (push polygon polygons)))
	       (make-model :position #(0 8 20) :vertices (make-array (list (length vertices)) :initial-contents vertices)
			   :objects (list (make-object :name "Cube" :polygons (make-array (list (length polygons)) :initial-contents polygons)
						       :color (make-array '(3) :initial-contents '(0 0.5 0.7)))))))

(setf (model-normal-vertices cube) (model-vertices cube)
      (model-texture-vertices cube) (model-vertices cube))

(loop for polygon below 12
	       do (loop for point below 3
			do (setf (point-normal-index (elt (polygon-points (elt (object-polygons (elt (model-objects cube) 0)) polygon)) point))
				 (point-position-index (elt (polygon-points (elt (object-polygons (elt (model-objects cube) 0)) polygon)) point))
				 (point-texture-index (elt (polygon-points (elt (object-polygons (elt (model-objects cube) 0)) polygon)) point))
				 (point-position-index (elt (polygon-points (elt (object-polygons (elt (model-objects cube) 0)) polygon)) point)))))
