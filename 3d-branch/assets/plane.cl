(defvar plane (let* ((vertices (list (make-vertex :values #(30.0 0.0 -30.0))
				     (make-vertex :values (make-array '(3) :initial-contents '(-30.0 0.0 -30.0)))
				     (make-vertex :values (make-array '(3) :initial-contents '(30.0 0.0 30.0)))
				     (make-vertex :values (make-array '(3) :initial-contents '(-30.0 0.0 30.0)))))
		     (polygons (list (make-polygon) ;;:a (nth 0 vertices) :b (nth 2 vertices) :c (nth 3 vertices))
				     (make-polygon))));; :a (nth 0 vertices) :b (nth 1 vertices) :c (nth 3 vertices)))))
		(setf (point-position-index (polygon-a (nth 0 polygons))) 3
		      (point-position-index (polygon-b (nth 0 polygons))) 2
		      (point-position-index (polygon-c (nth 0 polygons))) 1
		      (point-position-index (polygon-a (nth 1 polygons))) 2
		      (point-position-index (polygon-b (nth 1 polygons))) 1
		      (point-position-index (polygon-c (nth 1 polygons))) 0)
		(make-model :vertices (make-array (list (length vertices)) :initial-contents vertices)
			    :objects (list (make-object :name "Plane" :polygons (make-array (list (length polygons)) :initial-contents polygons)
							:position #(0 -1 0)
							:color (make-array '(3) :initial-contents '(0.0 0.8 0.5)))))))
(setf (model-y plane) -10)

(defvar plane-2 (let* ((vertices (list (make-vertex :values (make-array '(3) :initial-contents '(-1.0 1.0 1.0)))
				     (make-vertex :values (make-array '(3) :initial-contents '(1.0 1.0 1.0)))
				     (make-vertex :values (make-array '(3) :initial-contents '(-1.0 -1.0 0.0)))
				     (make-vertex :values (make-array '(3) :initial-contents '(1.0 -1.0 0.0)))))
		     (polygons (list (make-polygon) ;;:a (nth 0 vertices) :b (nth 2 vertices) :c (nth 3 vertices))
				     (make-polygon))));; :a (nth 0 vertices) :b (nth 1 vertices) :c (nth 3 vertices)))))
		(setf (point-position-index (polygon-a (nth 0 polygons))) 3
		      (point-position-index (polygon-b (nth 0 polygons))) 2
		      (point-position-index (polygon-c (nth 0 polygons))) 1
		      (point-position-index (polygon-a (nth 1 polygons))) 2
		      (point-position-index (polygon-b (nth 1 polygons))) 1
		      (point-position-index (polygon-c (nth 1 polygons))) 0)
		(make-model :vertices (make-array (list (length vertices)) :initial-contents vertices)
			    :objects (list (make-object :name "Plane" :polygons (make-array (list (length polygons)) :initial-contents polygons)
							:color (make-array '(3) :initial-contents '(0.5 0.5 0.5)))))))

(setf (model-normal-vertices plane-2) (model-vertices plane-2)
      (model-texture-vertices plane-2) (model-vertices plane-2))

(loop for polygon below 2
	       do (loop for point below 3
			do (setf (point-normal-index (elt (polygon-points (elt (object-polygons (elt (model-objects plane-2) 0)) polygon)) point))
				 (point-position-index (elt (polygon-points (elt (object-polygons (elt (model-objects plane-2) 0)) polygon)) point))
				 (point-texture-index (elt (polygon-points (elt (object-polygons (elt (model-objects plane-2) 0)) polygon)) point))
				 (point-position-index (elt (polygon-points (elt (object-polygons (elt (model-objects plane-2) 0)) polygon)) point)))))


(setf plane #S(MODEL
   :POSITION #(0.0 -1 0.0)
   :ROTATION #(0.0 0.0 0.0)
   :SCALE #(1.0 1.0 1.0 1.0)
   :VERTICES #(#S(VERTEX :VALUES #(30.0 -2.0 -30.0))
               #S(VERTEX :VALUES #(-30.0 -2.0 -30.0))
               #S(VERTEX :VALUES #(30.0 -2.0 30.0))
               #S(VERTEX :VALUES #(-30.0 -2.0 30.0)))
   :NORMAL-VERTICES NIL
   :TEXTURE-VERTICES NIL
   :USE-VAO T
   :OBJECTS (#S(OBJECT
                :NAME "Plane"
                :POSITION #(0 -1 0)
                :ROTATION #(0.0 0.0 0.0)
                :SCALE #(1.0 1.0 1.0 1.0)
                :MATERIAL NIL
                :SMOOTHING T
                :COLOR #(0.0 0.8 0.5)
                :POLYGONS #(#S(POLYGON
                               :POINTS #(#S(POINT
                                            :POSITION-INDEX 3
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL)
                                         #S(POINT
                                            :POSITION-INDEX 2
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL)
                                         #S(POINT
                                            :POSITION-INDEX 1
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL)))
                            #S(POLYGON
                               :POINTS #(#S(POINT
                                            :POSITION-INDEX 2
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL)
                                         #S(POINT
                                            :POSITION-INDEX 1
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL)
                                         #S(POINT
                                            :POSITION-INDEX 0
                                            :NORMAL-INDEX NIL
                                            :TEXTURE-INDEX NIL))))
                :INFO NIL
                :SHADER-PROGRAMS NIL
                :IDX-LENGTH 12
                :VAO 1
                :IBO 2
                :VBO-IDS (1)
                :TEXTURE-ID NIL))
   :ANIMATION NIL
   :WEIGHTS NIL
   :FRAME 1.0
   :KEY 0
   :NAME NIL))
