Max amount of Textures:

122 -> 244

(assuming 512x512 images, with rgba channels, 512 mb can hold up to 488 textures)

max amount of vertices:

1,714,285 in 384 mb

857,142 in 192 mb

about 857 objects can be loaded onto the GPU at a time for a 192 mb limit on objects with up to 1,000 vertices
      (note: attempt to fit all objects into 300 triangles)

How about bones?
Bones are 4x4 matrix with 32 bit floats. Now, thats 16 x 32, which is 512 bytes

For 192 mb that gives us...375 thousand bones

So each object, with up to 1000 vertices, can have up to 375 bones per object

therefore, we will have a max of 37 bones per obj (model irl...right?)

So, 122 (512x512 img) tex, 857 (1k verts) objects, and 37 bones per obj
leaves us with
177 mb in GPU
this of course ignores indexing and such.
So, to avoid



Midpoint mirror algo:

x below midpoint : midpoint + (midpoint - x)
x above midpoint : midpoint - (x - midpoint)

midpoint = 128
A - x= 32 :: 128 + (128 -  32) = 224
B - x=196 :: 128 - (196 - 128) = 60
If min/max value is 0
   	   	 Max - X will get the reflection over the midpoint, no matter what the maximum value is

Need to make an API document of sorts.
Think DnD in LISP type of thing. A 'book' that goes through explains what's happening in the engine and what to call to get it to do those things
Loading/Freeing resources should be done by the engine, with a simple and clear API that is accessed by the programmer.



Engine is geared towards only 256 Megabytes of video memory being available

Various things need to be kept track of, such as number of vertices, number of bones, number of textures, number of shaders, etc.
Due to this, there will need to be limiters on everything

Weights: Only 2 per vertex. On import, check how many weights are used. Normalize to 2: Get all weights used, Figure out which ones have the least amount attached to them, get the total, div by 2 and add it to the 2 biggest weights

Things to implement:

Navigation Mesh (NavMesh)


Sub-systems:
  -Modifiers:
    -Duplicator - Duplicates an object at (x, y, z), is a list. "Compiles" it to an array.
    -"Array" - Just like blender's. It is specifically to help cut down on Gfx mem usage.
    	       So, the general idea is that this would be used within an editor.
	       The model (a single mesh or a collection of meshes) is loaded first. Next, the modifier activates, rerendering the model with a modified location. Given the locs of [(0, 0, 0) {0, 10, 0) (5, 16, 10)] the model's vertices are rendered at (vertex-x/y/z + 0/0/0) (vertex-x/y/z + 0/10/0) (vertex-x/y/z + 5/16/10)
		   There will also be a rotational aspect to this.



Notes on landscaping:
   -In blender, create the landscape (how the land is supposed to look)
   -Next, set up tiling for each 'type' of tile (i.e. tiling for snow, dirt, grass, paths, etc.)
   -A possible couple ways to do it would be:
     -A: Having a mask per tile type (great for a few types and blending, but bad for memory usage)
     -B: Having a single mask layer that uses specific colors for specific tiles (great for many types and memory usage, but bad for blending)
     -So, we need to have a C, one that works great for many types, memory usage and blending
      	  -So, we need to clamp the values. Using 8 tile types (from bottom up: dirt, gravel, rocky dirt, marsh, thatch, grass, snowy grass, snow):
	       	       	  	    	    	  -0-255: [0,31] dirt, [32, 63]...
						  -So, we want to have the middle be 100% opacity with no blending going on.
						       0-? is 0% to 100% opacity and (31 - offset) to 31 is 100 to 0
						       So, we get the range of 0 - ? and do 100/? to get the 'blending factor'
						   The general rule is
						       V > [8] and V < [24] then Op = 100
						       V > [24] then Op = (- 100 (- Max V))
						       V < [8] then Op = (- 100 (- V min))
						       
