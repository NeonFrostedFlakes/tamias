(defstruct camera
  (pos (vector .0 .0 .0))
  (rot (vector .0 .0 .0))
  (front (vector .0 .0 0.0))
  (target (vector .0 .0 -3.0)))

(defvar t-camera (make-camera))

(defvar camera-x 0)
(defvar camera-y 0)
(defvar camera-z 0)
(defvar world-loc #(0.0 0.0 0.0))

(defvar camera #(0.0 0.0 0.0))
(defvar camera-rot #(0.0 0.0 0.0))

(defvar garbage-timer 0)
(defvar nika nil)
(defvar socra nil)
(defvar test-tower nil)
(defvar filling 1)
(defvar alt-nika nil)
(defvar nika-roller nil)

(defvar robert nil)

(defvar world-rotation-x 0)
(defvar world-rotation-y 0)
(defvar world-rotation-z 0)
(defvar world-rotation-inc-x 0)
(defvar world-rotation-inc-y 0)
(defvar world-rotation-inc-z 0)
(defvar world-scale 1.0)

(defvar rotating nil)
(defvar rotate-z nil)
(defvar world-rotation (make-array '(3)))

(defvar light-r .8)
(defvar light-g .8)
(defvar light-b .8)

(defvar move-socra? nil)

(defvar camera-matrix (make-mat4))
(defvar object-matrix (make-mat4))
(defvar perspective-matrix (make-mat4))

(defvar *vert-shader*
  "#version 130
void main()
{	

	// the following three lines provide the same result

	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
//	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
//	gl_Position = ftransform();
gl_FrontColor = gl_Color;
gl_FrontSecondaryColor = gl_SecondaryColor;
gl_BackColor = gl_Color;
gl_BackSecondaryColor = gl_SecondaryColor;

}

 ")

;;Simple world viewer, lol
(setf *vert-shader* "#version 130

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;
uniform vec3 model_loc;
// uniform vec3 model_rot;
// uniform vec4 model_scale;
uniform vec3 rotation_vec;
uniform vec3 translate_vec;
uniform mat4 perspective_mat;
uniform vec3 color;
out vec4 colour;

void main(){
vec3 rot_vec = radians(rotation_vec);
 
mat3 rot_mat_x = mat3(1.0, 0, 0,
0, cos(rot_vec.x), -sin(rot_vec.x),     
0, sin(rot_vec.x), cos(rot_vec.x));

mat3 rot_mat_y = mat3(cos(rot_vec.y), 0, sin(rot_vec.y),    
0, 1.0, 0,
-sin(rot_vec.y), 0, cos(rot_vec.y));

mat3 rot_mat_z = mat3(cos(rot_vec.z), -sin(rot_vec.z), 0,    
sin(rot_vec.z), cos(rot_vec.z), 0,
0, 0, 1.0);

mat4 rot_mat = mat4(transpose(rot_mat_x * rot_mat_y * rot_mat_z));

mat4 translate_mat = mat4(1.0);

translate_mat[3] = vec4(translate_vec * -1, 1.0);

mat4 idet_mat = mat4(1.0);

mat4 switch_mat = mat4(1.0);
switch_mat[1][1] = 0;
switch_mat[2][2] = 0;
switch_mat[1][2] = 1.0;
switch_mat[2][1] = 1.0;

mat4 mod_mat = mat4(1.0);
//mat4 mod_mat = mat4(.75);
mod_mat[3][3] = 1.0;

//mat4 pers_mat = switch_mat * transpose(switch_mat * perspective_mat);

vec4 vert_new = vec4(vertexPosition_modelspace, 1.0);

//vert_new = vert_new + vec4(translate_vec * -1, 0.0);
//vert_new[1] = vert_new[1] * -1;

//mat4 view_mat = transpose(rot_mat);

//mat4 pers_mat = mat4(1.0);
mat4 pers_mat = transpose(perspective_mat);

//vert_new[2] = vert_new[2] * -1;
vec4 norm_vec = vec4(0.0, 0.0, 0.0, 1.0);

colour = vec4(vertexPosition_modelspace,1.0);

// model_space_vert = model_loc + model_scale * model_rot * vertexPosition_modelspace;

// camera_space = rot_mat

vec3 camera_scale = vec3(0.4);

gl_Position = pers_mat * vec4(translate_vec * -1, 1.0) + rot_mat * mod_mat * vec4(model_loc * -1, 1.0) + vert_new;

//gl_Position = rot_mat * vec4(translate_vec * -1, 1.0) + mod_mat * vert_new;


}")

(setf *vert-shader* "#version 130

// Input vertex data, different for all executions of this shader.
in vec3 vertexPosition_modelspace;
uniform vec3 model_loc;
// uniform vec3 model_rot;
// uniform vec4 model_scale;
uniform vec3 camera_rot;
uniform vec3 translate_vec;
uniform mat4 perspective_mat;
uniform vec3 color;
uniform vec3 world_rot;
uniform vec3 world_loc;
//uniform mat4 view_cube;
out vec4 colour;

void main(){
vec3 rot_vec = radians(camera_rot);

vec4 trans_vec = vec4(translate_vec * -1, 1.0);

//float cosP = cos(rot_vec.x);
//float sinP = sin(rot_vec.x);
//float cosY = cos(rot_vec.y);
//float sinY = sin(rot_vec.y);

//vec3 camX = vec3(cosY, 0, -1 * sinY);
//vec3 camY = vec3(sinY * sinP, cosP, cosY * sinP);
//vec3 camZ = vec3(sinY * cosP, -1 * sinP, cosP * cosY);

//mat4 view_mat;
//view_mat[0] = vec4(camX, -1 * dot(camX, translate_vec));
//view_mat[1] = vec4(camY, -1 * dot(camY, translate_vec));
//view_mat[2] = vec4(camZ, -1 * dot(camZ, translate_vec));
//view_mat = transpose(view_mat);

mat3 rot_mat_x = mat3(1.0, 0, 0,
0, cos(rot_vec.x), -sin(rot_vec.x),     
0, sin(rot_vec.x), cos(rot_vec.x));

mat3 rot_mat_y = mat3(cos(rot_vec.y), 0, -sin(rot_vec.y),    
0, 1.0, 0,
sin(rot_vec.y), 0, cos(rot_vec.y));

mat3 rot_mat_z = mat3(cos(rot_vec.z), -sin(rot_vec.z), 0,    
sin(rot_vec.z), cos(rot_vec.z), 0,
0, 0, 1.0);

mat4 rot_mat = mat4(rot_mat_x * rot_mat_y);

mat4 translate_mat = mat4(1.0);

mat4 mod_mat = mat4(1.0);
mod_mat[3][3] = 1.0;

vec4 vert_new = vec4(vertexPosition_modelspace, 1.0);

mat4 pers_mat = perspective_mat;

vec4 norm_vec = vec4(0.0, 0.0, 0.0, 1.0);

colour = vec4(color, 1.0);

vec4 model_vert = vec4(model_loc,1.0) + vert_new; 

vec4 cam_vert = trans_vec + model_vert;

vec4 pers_pos = pers_mat * rot_mat * cam_vert;

gl_Position = pers_pos;


}")

(defvar *frag-shader*
  "#version 130
//precision highp float;
//in vec4 colour;
in vec4 colour;
out vec4 fragcolor;

void main()
{
	fragcolor = colour;
}")
#|
(setf *frag-shader*
        "#version 120
//precision highp float;
//in vec4 colour;
uniform vec4 colour;

void main()
{
	gl_FragColor = colour;
}")
|#

(defvar shader-prog nil)

(load "importers/importer.cl")

(defvar polygon-count 0)

(load "matrices-for-3d.cl")


;;(setf (model-key nika) 0)

(defvar nika-timer 0)

(defvar nika-bent-jump nil)

(defvar bones-printed? nil)

;;(incf (model-z nika-roller) -3)

(load "3d-drawing-function.cl")



(setf bones-printed? nil)

(defvar anim-running? t)
(setf anim-running? nil)

(defvar model-view (make-mat4))
(defvar model-key 0)
