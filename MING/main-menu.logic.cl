;;get to level
;;exit
;;start music

;;go from init to top
;;in init, start music, then go to top

#|
So, states allow for "higher-level" control flow
That is, control flow for what "segment" is being processed

the sub-state is the "offset" for that segment
where init is "offset 0", idle is "Offset 1", and so on (where each sub-state is offset X, decided by the user/developer). This is more the theoretical side of things, rather than the technical side of things
|#
(defun quit-game () (setf exit-tamias? t))

(state.init-ui main-menu top)

(ui.add-lister main-menu top
    ((round (- (/ tamias:screen-width 2) (* (length "Start Game") .5 char-width)))
     (round (- (/ tamias:screen-height 2) (* 5 .5 (+ 2 char-height))))
     (* (+ (length "Start Game") 1) char-width)
     (* char-height 5))
    ("Start Game" "Scores" "Options"
		  "Help" "Exit"))

(defun mm.lister! ()
  (let ((element (get-ui-el (car (ui-manager-collection-ids (get-ui-manager :main-menu :top)))
			    :main-menu :top)))
    (ui.set-active element)
    (ui.mouse.click element :lister)))

(define-symbol-macro
    main-menu-selector
    (ui-lister-selector (get-ui-el (car (ui-manager-collection-ids (get-ui-manager :main-menu :top)))
				   :main-menu :top)))
(states:add-logic
    (main-menu init)
    (start-music main-menu-music)
    (set-sub-state top))

(states:add-logic
    (main-menu top)
    (mm.lister!))

(defun confirm-selection-main-menu ()
  (case main-menu-selector
    (0 (states:set-state level init))
    (1 (states:set-sub-state scores-init main-menu))
    (2 (states:set-sub-state options main-menu))
    (3 (states:set-sub-state help main-menu))
    (4 (quit-game))))

(states:add-mouse
    (:left :down) (main-menu top)
  (mm.lister!)
  ;;(setf current-ui for current state to ui-lister)
  )

#|
(states:add-state-kb-key
    :up :down (main-menu top)
  (selection-cursor-up main-menu-selector 0 t))
(states:add-state-kb-key
    :down :down (main-menu top)
  (selection-cursor-down main-menu-selector 4 t))
|#

(states:add-state-kb-key
    :z :down (main-menu top)
    (confirm-selection-main-menu))
(states:add-state-kb-key
    :return :down (main-menu top)
    (confirm-selection-main-menu))

(states:add-state-kb-key
    :x :down (main-menu options)
  (states:pop-sub-state main-menu))
(states:add-state-kb-key
    :x :down (main-menu scores)
  (states:pop-sub-state main-menu)
  (states:pop-sub-state main-menu))
(states:add-state-kb-key
    :x :down (main-menu help)
  (states:pop-sub-state main-menu))

(defvar scores '(("BCB" 1994) ("Neon Frost" 2023) ("Marx" 1848) ("Lenin" 1917)))

(states:add-logic
    (main-menu scores-init)
    (sort scores #'> :key #'second)
    (set-sub-state scores))

(defun render-scores ()
  (let* ((name-x (round (/ tamias:screen-width 3)))
	 (score-x (round (* 1.6 name-x)))
	 (ns-y (round (/ tamias:screen-height 3)))
	 ;;ns-y stands for name-score-y, meaning it's where the current name/score pair is
	 (max-width 1)
	 (inc-y (+ char-height 2)))
    (let ((name-x-len (+ (* (length "Neon Frost") 16) name-x)))
      (if (> name-x-len score-x)
	  (setf name-x (- name-x (- name-x-len score-x)))))
    (setf max-width (- score-x name-x))
    (render:bg-element name-x ns-y max-width char-height tamias.colors:+dark-pastel-gray+)
    (render:bg-element score-x ns-y max-width char-height tamias.colors:+dark-steel-blue+)
    (render:text "Name" name-x ns-y)
    (render:text "Score" score-x ns-y)
    (incf ns-y inc-y)
    (loop for score in scores
	  do (render:bg-element name-x ns-y max-width char-height tamias.colors:+dark-pastel-gray+)
	     (render:bg-element score-x ns-y max-width char-height tamias.colors:+dark-steel-blue+)
	     (render:text (car score) name-x ns-y)
	     (render:text (write-to-string (cadr score)) score-x ns-y)
	     (incf ns-y inc-y))
    (incf ns-y inc-y)
    (render:bg-element (- name-x (* char-width 2)) ns-y
		       (* char-width (1- (length "x: to go back to main-menu")))
		       char-height
		       ;;tamias.colors:+dark-natural-green+
		       tamias.colors:+dark-magenta+
		       )
    (render:text "x: go back to main-menu" (- name-x char-width) ns-y)
    ))
    ;;loop for y below n by a
    ;;score-x = ?, name-x = ?
    ;; Where should these be?
    ;;
    #|
Layout

  Name   Score
  .
  .
  .
  .
  Type 'x' to go back
    |#

(states:add-render
    (main-menu scores)
    (render-scores))


#|
  		 (0 (start-game));start game
		 (1 (setf sub-state 'scores))
		 (2 (setf sub-state 'options));options))
		 (3 (setf sub-state 'help));options
		 (4 (quit-game));exit game
|#


