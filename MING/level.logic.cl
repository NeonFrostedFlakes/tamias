(states:def-logic (level init)
		  ;;load music and play it
		  ;;...maybe load music in (main-menu :init) instead?
		  (delete-ming-title)
		  (setf enemies nil)
		  (spawn-player)
		  (gen-wavelist)
		  (states:set-sub-state idle))

(defun process-group (group)
  (loop :for ship :in (enemy-group-list group)
	:do (ship-move ship)
	    (case (ship-state ship)
	      (:dead ;;optimization: use the ship-type to generate explosions on death
	       (setf (elt (enemy-group-list group)
				(position ship (enemy-group-list group)))
			   nil))
	      (:killed nil);;increase player score and stuff
	      (otherwise
	       (if (enemy-dead? ship)
		   (setf (elt (enemy-group-list group)
			      (position ship (enemy-group-list group)))
			 nil)
		   (ship-fire ship)))))
  (remove-nil (enemy-group-list group))
  (timer:increment spawn-timer)
  (when (timer:end? spawn-timer)
    (if (not (enemy-group-last? group))
	(spawn)
	(progn (incf (player-waves player))
	       (gen-wavelist)
	       (if (not (enemy-group-list group))
		   (setf enemies (remove group enemies))))))
  (if (not (enemy-group-list group))
      (if (not (enemy-group-last? group))
	  (setf enemies (remove group enemies))
	  (progn (incf (player-waves player))
		 (gen-wavelist)
		 (setf enemies (remove group enemies))))))

(defun process-ships ()
  ;;process enemies
  (if (not enemies)
      (spawn))
  (loop :for group :in enemies
	:do (when group
	      (process-group group)))
  ;;process player
  (ship-move player) (ship-fire player)
  (when (player-missile-fired? player)
    (when (fire-timer (player-missile-timer player))
      (setf (player-missile-fired? player) nil))))


(defun render-group (group)
  (loop :for ship :in (enemy-group-list group)
	:do (render-ship ship)))

(defun render-ships ()
  (loop :for group :in enemies
	:do (when group
	      (render-group group)))
  (render-ship player))

(defun render-bullets ()
  (loop :for blt :in bullets
	:do (render-bullet blt))
  (loop :for blt :in player-bullets
	:do (render-bullet blt)))

(defun render-explosions ()
  (render-particles explosions))

(states:add-logic (level idle)
		  (process-bullets)
		  (process-ships)
		  (process-explosions))

(defun render-player-info ()
  (render:box 0 (- tamias:screen-height 40)
	      tamias:screen-width 40
	      tamias.colors:+aqua+)
  (render:box 0 (- tamias:screen-height 36)
	      tamias:screen-width 32
	      tamias.colors:+black+)
  (render:text "HP: "
	        0 (- tamias:screen-height 32))
  (render:text (write-to-string (player-hp player))
	       (* (length "HP: ") char-width)
	       (- tamias:screen-height 32))
  (let ((player-waves-str (write-to-string (player-waves player)))
	(waves-str "Wave: "))
    (render:text waves-str
		 (- tamias:screen-width (* (+ (length waves-str) (length player-waves-str))
					   char-width))
		 (- tamias:screen-height 32))
    (render:text "Groups: "
		 (round (- (/ tamias:screen-width 2)
			   (* (length "Groups: ") (/ char-width 2))))
		 (- tamias:screen-height 32))
    (render:text (write-to-string (length wavelist))
		 (round (+ (/ tamias:screen-width 2)
			   (* (length "Groups: ") (/ char-width 2))))
		 (- tamias:screen-height 32))    
    (render:text player-waves-str 
		 (- tamias:screen-width (* (length player-waves-str) char-width))
		 (- tamias:screen-height 32))))


(states:add-render
    (level idle)
    ;;render-bg here
    (render-ships)
    (render-explosions)
    (render-bullets)
    (render-player-info))
