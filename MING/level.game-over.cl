(defvar game-over-timer (timer:make :end 5 :in-seconds? t))
(states:add-logic
    (level dead)
    (timer:increment game-over-timer)
    (when (timer:end? game-over-timer)
      (set-sub-state game-over-init)))

(states:add-logic
    (level game-over-init)
    (free-sounds)
    (set-sub-state game-over))

(state.init-ui level game-over)
(ui.add-label level game-over
	      (round (/ tamias:screen-width 2))
	      (round (/ tamias:screen-height 2))
"   Game Over!

Press Z or Enter"
	      :color tamias.colors:+dark-red+
	      :text-color tamias.colors:+black+
	      :width (round (/ tamias:screen-width 4))
	      :height (round (/ tamias:screen-height 4))
	      :scale-text t)



