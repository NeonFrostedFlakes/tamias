(defvar type-a-angle 180)
(defmethod render-ship ((ship type-a))
  (let ((flip (if (> (ship-speed-x ship) 0)
		  :horizontal :none))
	(angle (* (/ (abs (ship-speed-x ship)) (ship-speed-x ship))
		  -20)))
    (render:sprite (ship-sprite-sheet ship) (ship-x ship) (ship-y ship)
		   :flip flip :angle (+ angle type-a-angle))))

(defmethod ship-fire ((ship type-a))
  (when (within-width (ship-x ship) (player-x) 64)
    (when (fire-timer (ship-fire-timer ship))
      (spawn-bullet ship))))

(defmethod ship-move ((ship type-a))
  ;;  :a -Diagonal - slow ("bounces" off edge of "screen"),
  ;;     shoots when 1/4 of screen is left, shoots down
  ;;  TYPE A: Vectors: +y at a rate of 0.5, +/- x at a
  ;;     rate of 0.25 until edge of the screen then change 'sign'
  ;;move ship then test player <-> ship collision
  (when (edge-of-screen ship)
    (setf (ship-speed-x ship) (* -1 (ship-speed-x ship))))
  (incf (ship-y ship) (ship-speed-y ship))
  (incf (ship-x ship) (ship-speed-x ship))
  (when (test-bb-collision ship player)
    (generate-explosions (ship-x ship) (ship-y ship)
			 8 (/ tamias:fps 5) 2 2 :player-hit)
    (kill-ship ship)
    (setf (player-invincible player) t))
  (if (> (+ (ship-y ship) (ship-height ship)) (+ tamias:screen-height (* 2 (ship-height ship))))
      (setf (ship-state ship) :dead)
      (when (bullet-collide? ship)
	(when (<= (ship-hp ship) 0)
	  (kill-entity ship)))))

