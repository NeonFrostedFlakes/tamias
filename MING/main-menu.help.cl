;;will display the 'information' on the help screen
(defvar txt-group nil)
(defvar help-string "Ah, hello there! Welcome to
MING: Ming Is Not Galaga. :)

Default controls:
Z: auto fire/confirm
X: Previous
C: Fire Missile
Space bar: manual fire
Enter: Pause/confirm")
(defun render-help-text ()
  (let ((rend-str-group nil)
	(text-limit (round (/ (- tamias:screen-width (/ tamias:screen-width 4)) 16)))
	(y (round (/ tamias:screen-height 3))))
    (setf rend-str-group (tamias.string:text-chunker help-string text-limit)
	  txt-group rend-str-group)
    (loop :for str :in rend-str-group
	  :do (if (> (length str) 0)
		  (render:box (round (/ tamias:screen-width 8))
			      (+ y 1)
			      (- tamias:screen-width (/ tamias:screen-width 4))
			      (- char-height 2)
			      (tamias.colors:offset tamias.colors:+dark-magenta+ -20)))
	      (render:text str
			   (round (/ tamias:screen-width 8))
			   y)
	      (incf y char-height))))

(states:add-render
    (main-menu help)
    (render-help-text))
