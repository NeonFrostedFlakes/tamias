;;(defvar firing nil) ;;player-code


(defun allow-missile ()
  (setf (player-missile-fired? player) nil))


(defun set-speed (x-dir)
  (let ((move-state :idle))
    (if (> 0 x-dir)
	(setf move-state :right)
	(setf move-state :left))
    (if (and (not (eq (player-move-state player) move-state))
	     (not (eq (player-move-state player) :slowing)))
	(setf move-state :rl))
;;    (print move-state)
;;    (print (player-move-state player))
  (setf (player-move-state player) move-state
	(player-speed-x) (floor (* x-dir (/ tamias:screen-width tamias:fps))))))

(defun rl-move-state (x-dir) ;;rl-move-state is for when the player's move state is :rl
  (if (eq x-dir :right)
      (progn (setf (player-move-state player) :left)
	     (set-speed -1))
      (progn (setf (player-move-state player) :right)
	     (set-speed 1))))

(states:add-state-kb-key
    :return :down (level idle)
  (states:set-sub-state paused))

(states:add-state-kb-key
    :left :down (level idle)
  (set-speed -1))

(states:add-state-kb-key
    :left :up (level idle)
  (setf (player-move-state player) :slowing))
  
(states:add-state-kb-key
    :right :down (level idle)
  (set-speed 1))

(states:add-state-kb-key
    :right :up (level idle)
  (setf (player-move-state player) :slowing))




(states:add-state-kb-key
    :z :down (level idle)
  (start-firing))
;;  (setf (tamias.entities:player-firing player) t))
;;  (setf (tamias.entities:player-firing player) nil))
(states:add-state-kb-key
    :space :down (level idle)
  (start-firing))
(states:add-state-kb-key
    :space :up (level idle)
  (stop-firing))

(defun fire-missile ()
  (when (not (player-missile-fired? player))
      (entity-fire-missile player)))

(states:add-state-kb-key
    :c :down (level idle)
  (fire-missile))
#|
(states:add-state-kb-key
    :c :up (level idle)
  (allow-missile player))
|#

(states:add-state-kb-key
    :return :down (level game-over)
  (quit-to-main-menu))

(states:add-state-kb-key
    :z :down (level game-over)
  (quit-to-main-menu))
