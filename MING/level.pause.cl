(state.init-ui level paused)

(states:add-render
    (level paused)
    (tamias:render-state level '(:idle)))


(ui.add-lister level paused
    ((round (- (/ tamias:screen-width 2) (* (length "Return to main-menu") .5 char-width)))
     (round (- (/ tamias:screen-height 2) (* 5 .5 (+ 2 char-height))))
     (* (+ (length "Return to main-menu") 1) char-width)
     (* char-height 5))
    ("Resume Game" "Options"
		  "Help" "Return to main-menu"))

(defun lp.lister! ()
  (let ((element (get-ui-el (car (ui-manager-collection-ids (get-ui-manager :level :paused)))
			    :level :paused)))
    (ui.set-active element)
    (ui.mouse.click element :lister)))

(define-symbol-macro
    level-selector
    (ui-lister-selector (get-ui-el (car (ui-manager-collection-ids (get-ui-manager :level :paused)))
				   :level :paused)))

(states:add-logic
    (level paused)
    (lp.lister!))

(defun confirm-selection-level ()
  (case level-selector
    (0 (states:previous-sub-state))
    (3 (switch-track-to main-menu-music)
     (load-ming-title)
     (states:set-state main-menu top))
    ))

(states:add-state-kb-key
    :z :down (level paused)
  (confirm-selection-level))

(states:add-state-kb-key
    :return :down (level paused)
  (confirm-selection-level))

(states:add-state-kb-key
    :x :up (level paused)
  (states:up-sub-state))

(states:add-state-kb-key
    :x :up (level options)
  (states:up-sub-state))

(states:add-state-kb-key
    :x :up (level scores)
  (states:up-sub-state))

