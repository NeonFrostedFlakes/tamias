(states:define-state credits)

;;Show credits, loooooooooool
;;Literally just me

;;But, that doesn't mean we can't include the tools I used :)

;;So: Emacs, Common Lisp, Slime, Emacs-company mode, Treemacs, Reaper
;;
;;VSTs: Aelita (effectively everything except noise), Polivoks Station, Polyakov
;;I don't think I used the Alice 1377 with this project, though I may have

(defvar credits-text
"             Program Lead
     Neon Frost - Brandon Blundell



            Art Director
     Neon Frost - Brandon Blundell




            Pixel Artist 
     Neon Frost - Brandon Blundell



             Composer
     Neon Frost - Brandon Blundell



           Sound Effects
     Neon Frost - Brandon Blundell



 Tools Used In The Production Of MING



Linux - Xubuntu distribution


Emacs - rectangle editing - company mode
      - SLIME (Superior Lisp Interaction
        Mode for Emacs) 
      - Treemacs 


SDL2 libraries


Common LISP - SBCL implementation

            - CL-SDL2 CL-SDL2-IMAGE
              CL-SDL2-MIXER CL-SDL2-TTF

            - Tamias


DAW (Digital Audio Workstation): REAPER

VSTs: Aelita, Polivoks Station, Polyakov

Sounds: Various drum sounds developed on
        Moogs, Korgs, and so on



GIMP


Krita


mtPaint
")

(setf credits-text (tamias.string:text-chunker credits-text))

(defvar credits-y (+ tamias:screen-height
		     (* char-height 4)))
(defvar credits-color tamias.colors:+aqua+)

(defun render-credits ()
  (let ((c-y-inc 0))
    (loop :for txt :in credits-text
	  :do  (render:text txt
			    0 (+ credits-y c-y-inc)
			    :color credits-color)
	       (incf c-y-inc 16))))

(states:add-logic
    (credits idle)
    (decf credits-y 2)
    (if (< credits-y (- (* (length credits-text) char-height)))
	(progn (set-state main-menu init)
	       (switch-track-to main-menu-music)
	       (setf credits-y (+ tamias:screen-height
				  (* char-height 4))))))
	
(states:add-render
    (credits idle)
    (render-credits))

(states:add-state-kb-key
    :down :down
    (credits idle)
    (decf credits-y 4))
