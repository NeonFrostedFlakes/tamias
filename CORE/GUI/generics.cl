(defgeneric ui.kb-input (ui-element ui-type kb-input)
  (:method (ui-element ui-type kb-input)
    ;;You can do some pretty cool things here :)
    (declare (ignore ui-element ui-type kb-input))
    nil))

(defgeneric ui.render (ui-element ui-type &optional hover x-offset y-offset)
  (:method (ui-element ui-type &optional hover x-offset y-offset)
    (declare (ignorable hover x-offset y-offset))
    nil))

(defgeneric ui.mouse.hover (ui-element ui-type)
  (:method (ui-element ui-type)
    (declare (ignore ui-element))
    (declare (ignore ui-type))
    nil))
  ;;draw the element again but lighter? Uhhh, something like that I guess?
  ;;Actually, yeah. Unless it's a light color, then use a darker color (in fact, +/- 40 dependent on </> 127)

(defgeneric ui.mouse.click (ui-element ui-type)
  (:method (ui-element ui-type)
    (declare (ignore ui-element))
    (declare (ignore ui-type))
    nil))
  ;;chekc mouse-x mouse-y, if action, funcall action, with ui-element as first arg
