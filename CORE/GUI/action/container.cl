(defun gui.window.click (window)
  "Windows are mini-windows in an application. They are movable, have scroll-bars, have title-bars and so on. Can be closed, functionality coming soon ^TM
  test where in the window it clicked. If it clicked in the area where teh scroll-bars are, then deal with the mouse moving (check if the previous-x and previous-y of the scroll bar has changed and change it by the incrementor * change-in-x/y)"
  (let ((clicked nil))
    clicked))

(defun gui.frame.click (frame)
  (let ((clicked nil))
    clicked))

(defun gui.dock.click (dock)
  "Docks are movable frames. They are movable and can be closed (hidden)."
  (let ((clicked nil))
    clicked))

(defun gui.container.click (container &optional (x-acc 0) (y-acc 0))
  (let ((p-x (ui-x container))
	(p-y (ui-y container))
	(skip-ui-ids nil))
    (setf skip-ui-ids
	  (case (ui-type container)
	    (:frame (gui.frame.click container))
	    (:window (gui.window.click container))
	    (:dock (gui.dock.click container))
	    (:container nil)
	    (otherwise t)))
    (if (and (not skip-ui-ids)
	     (mouse.test-position (+ x-acc p-x);;(ui-base-x container))
				  (+ y-acc p-y);;(ui-base-y container))
				  (ui-width container)
				  (ui-height container)
				  'box))
	(loop for id in (ui-ids container)
	      do (let ((child (gethash id (ui-children container))))
		   (if (ui-container-p child)
		       (gui.container.click child (+ p-x x-acc) (+ p-y y-acc))
		       (if (mouse.test-position (+ p-x x-acc (ui-x child))
						(+ p-y y-acc (ui-y child))
						(ui-width child)
						(ui-height child)
						'box)
		       	   (progn (setf mouse.ui-x (- tamias:*mouse-x* p-x)
					mouse.ui-y (- tamias:*mouse-y* p-y))
				  (ui.set-active child)
				  (action.prep child (ui-type child))))))))))

(document gui.window.click "When the window is clicked, it will check where it is clicked, if it's clicked within the 'title bar' area, then it will process the title bar click. Viewport, then viewport click. and scroll bars, scroll bar click.
An example window:

+-------------------+
|title          |^ X|
|-------------------|
|Ui-elements      |A|
|                 |I|
|                 |V|
|_________________| |
|<|=============|> +|
+-------------------+

")
