(defmacro scroll.horizontal (ui-el)
  )

(defmacro scroll.vertical (ui-el)
  `(if (< (cadr mouse.ui) 16)
       "scroll up"
       (if (> (cadr mouse.ui) (- (ui-element-height ,ui-el) 16))
	   "scroll down"
	   "Area not on scroll-bar?")))

(defmethod ui.mouse.click (ui-element (ui-type (eql :scroll-bar)))
  "Check Mouse-y against the ui-element. "
  (if (scroll-bar-horizontal? ui-element)
      (scroll.horizontal ui-element)
      (scroll.vertical ui-element)))

(defmethod ui.mouse.click (ui-element (ui-type (eql :button)))
  (eval (ui-element-action ui-element)))

(defmethod ui.mouse.click (ui-element (ui-type (eql :check-box)))
  '(setf (ui-check-box-checked ui-element) t)
  (eval (ui-element-action ui-element)))
