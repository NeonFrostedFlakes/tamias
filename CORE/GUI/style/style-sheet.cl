brush : (button width height function-on-click icon-path-or-index)
brush-size : (spin-box width height min max)
rectangle-select : (button width height function-on-click icon-path-or-index)

tools-frame : (vertical num-icon-per-row brush rectangle-select)

menu-bar { file : new (new-file) . open (open-file-dialog) }

(defun style-keyword (keyword)
  (case keyword
    ((:b :button) (make-button))
    ((:sb :spin-box) (make-spin-box))
    ((:f :frame) (make-frame))
    ((:w :window) (make-window))
    ))

(defun style.ui-set-props (ui-el properties)
  (case (ui-type ui-el)
    (window nil)
    (button nil)))

(defun open-style-sheet (ss-file &optional (state tamias:state) (sub-state (state-sub-state tamias:state)))
  (with-open-file (style-sheet ss-file)
    (let ((new-ui-el nil))
      (loop for obj = (read style-sheet nil nil)
	 while obj
	 do (case (type-of obj)
	      (cons (style.ui-set-props new-ui-el obj))
	      (keyword (if new-ui-el
			   (add-state-ui-element state sub-state new-ui-el))
		       (setf new-ui-el (style-keyword obj)))
	      )))))
