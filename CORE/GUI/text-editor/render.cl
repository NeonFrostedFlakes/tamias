(defmethod ui.render (ui-element (ui-type (eql :text-editor)) &optional hover (x-offset 0) (y-offset 0))
  (let ((ui ui-element))
    (let ((x (+ x-offset (ui-base-x ui)))
	  (y (+ y-offset (ui-base-y ui)))
	  (w (ui-base-width ui))
	  (h (ui-base-height ui))
	  (text-x (ui-editor-view-x ui))
	  (text-y (ui-editor-view-y ui))
	  (tw (ui-editor-view-width ui))
	  (th (ui-editor-view-height ui))
	  (scroll-x (scroll-bar-current-offset (ui-editor-scroll-h ui)))
	  (scroll-y (scroll-bar-current-offset (ui-editor-scroll-v ui)))
	  (str (text-entry-text (ui-entry ui)))
	  (edited (text-entry-edited (ui-entry ui)))
	  (text-struct (ui-element-entry ui)))
      (ui.render-bg ui)
      (if (eq current-text-context text-struct)
	  (render.text-cursor text-struct str x y))
      ;;create a text buffer for the ui-element if text has been edited
      (if edited
	  (progn (if (not (tamias-string-buffer (ui-label ui)))
		     (remove-buffer-on-quit ui))
		 (render:to-buffer str (tamias-string-buffer (ui-label ui)))))
      ;;render the text so that it's inside the entry element
      (ui.render-bg ui text-x text-y tw th x-offset y-offset (ui-editor-text-bg-color
							      ui))
      (let ((txt-dim (render:text-dimensions str)))
	(if (> h (cadr txt-dim))
	    (render:text-buffer (tamias-string-buffer (ui-label ui))
				(list scroll-x scroll-y
				      tw th)
				(list (+ text-x x) (+ text-y y)
				      (- tw (- tw (car txt-dim)))
				      (- th (- th (cadr txt-dim))))
				(ui-editor-text-color
				 ui))
	    (render:text-buffer (tamias-string-buffer (ui-label ui))
				(list scroll-x scroll-y
				      tw th)
				(list x y
				      (- tw (- tw (car txt-dim)))
				      h)
				(ui-editor-text-color
				 ui))))
      ;;source-rect will be offset by scrollbars
      ;;(ui.render-text-chunker str x y w h)
      (ui.render-scroll-bar (ui-window-scroll-h ui)
			    (+ (ui-x ui) x-offset)
			    (+ (ui-y ui) y-offset)
			    (ui-width ui)
			    (ui-height ui))
      (ui.render-scroll-bar (ui-window-scroll-v ui)
			    (+ (ui-x ui) x-offset)
			    (+ (ui-y ui) y-offset)
			    (ui-width ui)
			    (ui-height ui))
      (if hover
	  (render.cursor 4 18 (list 200 200 200 255))
	  ;;send message to mouse sub-system to switch to text cursor, that "I" thing
	  )
    )))
