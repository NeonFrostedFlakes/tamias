#|
Big note, unsure if exists already elsewhere:
Development of GUI boils down to, eventually, creating a window manager inside an application
Yes, you end up building some kind of implementation of i3, xfce, stumpwm, or etc. inside of the GUI lib
|#

(defmacro def-ui-alias (alias-fun fun-name)
  `(let ((eval-str ""))
     (setf eval-str (concatenate 'string "(defmacro " (write-to-string ',alias-fun)
				 " (instance) `(" (write-to-string ',fun-name) " ,instance))"))
     (eval (read (make-string-input-stream eval-str)))))

(defvar current-ui-id -1)

(defvar ui-managers-list nil)

(defstruct ui-manager
  (collection (make-hash-table) :type hash-table)
  (collection-ids nil :type list)
  (current-hover -1 :type integer)
  (current-active -1));;waffles between a ui-element and -1 

(def-ui-alias ui-collection-ids ui-manager-collection-ids)
(def-ui-alias ui-manager-ids ui-manager-collection-ids)
(def-ui-alias ui-mng-ids ui-manager-collection-ids)

(defvar ui-managers (make-hash-table))
(define-symbol-macro current-ui-state (states:state-symbol tamias:state))

(define-enum (ui-top-left 0) (ui-top-right 1)
  (ui-center 2) (ui-bottom-left 3)
  (ui-bottom-right 4))


#|
(defmacro def-ui-fun (rep-fun fun-name)
  `(defun ,rep-fun (element)
     (,fun-name `,element)))
|#

(defmacro eval-ui-macro-string (eval-str)
  `(eval (read (make-string-input-stream ,eval-str))))
	  
(defmacro get-ui-element (id state sub-state)
  `(gethash ,id (ui-manager-collection (gethash (aux:to-keyword ',sub-state) (gethash (aux:to-keyword ',state) ui-managers))))
  )

(defmacro get-ui-el (id state sub-state)
  `(gethash ,id (ui-manager-collection (gethash (aux:to-keyword ',sub-state) (gethash (aux:to-keyword ',state) ui-managers)))))


(defstruct number-entry
  (value (make-tamias-variable :type 'int :value 0) :type tamias-variable)
  (maximum 1 :type integer)
  (minimum 0 :type integer))

(defstruct ui
  (type :nil :type keyword)
  (ID (incf current-ui-id))
  x
  x-init
  y
  y-init
  width
  width-init
  height
  height-init
  click-offset-w
  click-offset-h
  (color (tamias.colors:offset tamias.colors:+black+))
  image
  image-path
  label
  use-buffer
  buff-func
  scrollable
  scale-text
  (origin ui-top-left)
  parent ;;useful for tabbing
;;  zoom
  ;;  (input (make-hash-table))
  action ;;stuff to do when element is clicked or otherwise activated, like (emit-message "button clicked!")
  custom ;;generic special (struct) slot, used for anything that's not an entry struct
  ;;in particaular: spin box values
  entry
  tool-tip ;;tamias-string or ui-label, both would be interesting
  )

(def-ui-alias ui-callback ui-action)
(def-ui-alias ui-activate! ui-action)
(def-ui-alias ui-activate-function ui-action)

(defstruct (ui-base
	     (:include ui)
	     ))
    
(defstruct (ui-element (:include ui-base))
 )

(def-ui-alias ui-element-callback ui-element-action)



(defstruct (ui-button (:include ui-element
		       (type :button)
		       (x 0)
		       (y 0)
		       (x-init 0)
		       (y-init 0)
		       (width 32)
		       (width-init 32)
		       (height 32)
		       (height-init 32)
		       (color '(0 0 0 255))))
  (button-state 0) ;;0 = released, 1 = pressed, 2 = hover
  )




(defstruct (ui-scroll-bar (:include ui-element
			   (type :scroll-bar)
			   (x 0)
			   (y 0)
			   (width 32)
			   (height 32)
			   (action 'scroll)
			   (scrollable t)
			   (color tamias.colors:+chalk-white+)))
  (incrementor 1) ;;how much the window goes up or down when the up/down arrows are clicked
  (max 1)
  (current-offset 0)
  placement ;; nil - right/bottom (default), 1 - left/top
  horizontal? ;;nil - vertical (default), 1 -  horizontal
  hidden?)
(def-ui-alias scroll-offset ui-scroll-bar-current-offset)

(defstruct (scroll-bar (:include ui-scroll-bar)))

(defstruct (ui-spin-box (:include ui-element
				  (type :spin-box)
				  (custom (make-number-entry))))
  (min-value 0 :type integer)
  (max-value 10 :type integer)
  (incrementer 1))



(defstruct lister-obj
  str
  type
  callback)

(defun default-ui-lister-cb-sym (obj)
  (format t "lister obj str: ~A type: ~A" (lister-obj-str obj) (lister-obj-type obj)))

(defstruct (ui-lister (:include ui-element
				(type :lister)))
  children
  (selector 0)
  (total-objs 0)
  (callback-sym 'default-ui-lister-cb-sym)
  selector-active?
  (selector-color tamias.colors:+pastel-blue+))

(def-ui-alias ui-lister-current ui-lister-selector)
(def-ui-alias ui-lister-child-objects ui-lister-children)

(defun get-current-lister-object (lister)
  (elt (ui-lister-child-objects lister) (ui-lister-current lister)))


(defstruct (text-entry (:include tamias-text
			(edited t)))
  (display t))

(defstruct (ui-cursor (:include ui-element))
  (timer 0)
  (max-time 30)
  blinking?)

(defstruct (ui-text (:include ui-element
		     (type :entry)
		     (label (make-tamias-string :text ""))
		     (entry (make-text-entry))
		     (click-offset-w 32)
		     (click-offset-h 32)
		     ))
  (cursor (make-ui-cursor)))

(defstruct (ui-entry (:include ui-text)))

;;MASSIVE NOTE

;;on creation, ui-text/entry needs to have
;;well, no longer massive note
;;...needs to have the click-offset-h/w set to whatever would be apporopite lol
;;fuck me

"Labels are meant to be one-line, non-editable (client side), 'text fields'"
(defstruct (ui-label (:include ui-element
		      (type :label)
		      (label (make-tamias-string :text "Label"))))
  (text-color tamias.colors:+white+)
  hidden)

"Text Fields are meant to be multi-line 'labels'"
(defstruct (ui-text-field (:include ui-label
				    (type :text-field))))

(defstruct (ui-tf (:include ui-text-field)))



(defstruct node-label
  )

(defstruct (ui-node (:include ui-element
			      (type :node)
			      (x 0)
			      (x-init 0)
			      (y 0)
			      (y-init 0)
			      (width (* 16 32))
			      (width-init (* 16 32))
			      (height (* 16 32))
			      (height-init (* 16 32))
			      ))
  (labels (make-hash-table))
  input-nodes
  output-nodes)



(defstruct (ui-container (:include ui-element))
  (border-color tamias.colors:+black+)
  children
  children-ids
  (pack-offset #(0 0))
  (child-padding #(0 0)))
;;Pack-offset uses the width and height of ui-elements
;;

(def-ui-alias ui-ids ui-container-children-ids)
(def-ui-alias ui-children-ids ui-container-children-ids)
(def-ui-alias ui-ids-list ui-container-children-ids)
(def-ui-alias ui-children ui-container-children)
(def-ui-alias ui-padding ui-container-child-padding)

(defmacro ui-padding-x (instance)
  `(aref (ui-padding ,instance) 0))
(defmacro ui-padding-y (instance)
  `(aref (ui-padding ,instance) 1))

(defmacro ui-pack-offset-x (instance)
  `(aref (ui-container-pack-offset ,instance) 0))
(defmacro ui-pack-offset-y (instance)
  `(aref (ui-container-pack-offset ,instance) 1))



#|
(setf (fdefinition 'ui-children-ids) #'ui-container-children-ids)
(setf (fdefinition 'ui-ids) #'ui-container-children-ids)
(setf (fdefinition 'ui-ids-list) #'ui-container-children-ids)
(setf (fdefinition 'ui-children) #'ui-container-children)
|#

(defstruct (ui-console (:include ui-container))
  )

#|
Spot where window code was
|#



(defstruct (ui-dock (:include ui-container
		     (type :dock)
		     (children (make-hash-table)))))


(defstruct (ui-frame (:include ui-container
		      (type :frame)
		      (x 0)
		      (x-init 0)
		      (y 0)
		      (y-init 0)
		      (width tamias:screen-width)
		      (width-init tamias:screen-width)
		      (height tamias:screen-height)
		      (height-init tamias:screen-height)
		      (children (make-hash-table)))))
(def-ui-alias ui-frame-ids ui-frame-children-ids)

  

;;WHile I recognize having everything in one struct isn't the best idea, mainly spatial concerns, I would rather have 10 megabytes of wasted space than have to deal with RSI

(defstruct (ui-menu (:include ui-container
		     (type :menu)
		     (children (make-hash-table))))
  (widest 0)
  active? ;;temp code because I'm a dumb slut
  active-item ;;active? is a symbol
  ;;hash table of ui-menu-item(s)
  )
(def-ui-alias ui-menu-ids ui-menu-children-ids)

(defstruct (ui-menu-item (:include ui-menu
				   (type :menu-item)
				   (x 0)
				   (y 0)))
  )
(def-ui-alias ui-menu-item-ids ui-menu-item-children-ids)

;;ui-menu-item-action will be a macro that is just (ui-element-special ui-menu-item)

(defstruct (ui-menu-bar (:include ui-menu
			 (type :menu-bar)
			 (x 0)
			 (x-init 0)
			 (y 0)
			 (y-init 0)
			 (width tamias:screen-width)
			 (width-init 'tamias:screen-width)
			 (height (cadr tamias.string:character-size))
			 (height-init '(cadr tamias.string:character-size))
			 (child-padding #(4 4))
			 ))
  hidden);;Symbol
(def-ui-alias ui-menu-bar-ids ui-menu-bar-children-ids)


(defmacro ui.re-init (id state sub-state)
  `(symbol-macrolet ((el (get-ui-el ,id ,state ,sub-state )))
     (setf (ui-x el) (eval (ui-x-init el))
	   (ui-y el) (eval (ui-y-init el))
	   (ui-width el) (eval (ui-width-init el))
	   (ui-height el) (eval (ui-height-init el)))))
			      

;;ui-frame holds ui-elements

#|
(defun get-current-active-menu (ui-menu)
  ;;recursively get the current active menu-item structure
  ;;So, assuming that File is active with no other active menu-item's menu being active, then the File menu would be returned as being active, along with it's x y widht and height
  (let ((current-active-menu ui-menu) ;;what we want is the current active menu item, not the menu
	(x (ui-x ui-menu))
	(y (ui-y ui-menu))
	(width (ui-width ui-menu))
	(height (ui-height ui-menu)))
    (loop for id in (ui-menu-ids ui-menu)
       do (let ((ui-mi-menu (gethash id (ui-menu-children ui-menu))))
	    ;;ui-mi-menu = ui-menu-item-menu
	    (if ui-mi-menu
		(if (ui-menu-active-item ui-mi-menu)
		    (progn (setf current-active-menu (gethash id (ui-menu-children ui-menu)))
			   (incf x (ui-width ui-menu))
			   (incf y (* (position id (ui-menu-ids ui-menu)) (cadr tamias.string:character-size)))
			   (setf width (ui-width ui-mi-menu)
				 height (ui-height ui-mi-menu))
			   (let ((new-active nil)
				 (n-x 0)
				 (n-y 0)
				 (n-w 0)
				 (n-h 0))
			     (setf (values new-active n-x n-y n-w n-h) (get-current-active-menu ui-mi-menu))
			     (if new-active
				 (progn (setf current-active-menu new-active)
					(incf x n-x)
					(incf y n-y)
					(setf width n-w
					      height n-h))))
			   (values current-active-menu x y width height))))))
    ))
|#

;;Note on future docker implementation:
;;  X-equation and such are just the x and y location.


;;If you are adding a ui-element to a ui-frame, just use numbers in relation to the offset of the ui-frame
;;So, if you want to draw a ui-button inside a ui-frame and the x/y of ui-button is 0/0 in relation to ui-frame, then just use 0/0


(defmacro load-image.ui (ui image-path)
  `(if (probe-file ,image-path);;if it exists, then load it
       (setf (ui-image-path ,ui) ,image-path
	     (ui-image ,ui) (tamias:load-image ,image-path));;(sdl2:create-texture-from-surface tamias:renderer (sdl2-image:load-image image-path)))
       (tamias:console.add-message (combine-strings "Sorry, either the image doesn't exist or the path to it is wrong. Current path: " image-path "
Example: font.png is \"engine/graphics code/font.png\" not Game/font.png or assets/font.png."))))
	

#|(defun ui.render-elements ()
(ui.render ui-menu-bar 'menu-bar)
(loop for ui-element in (gethash current-element elements)
   do (ui.render ui-element (ui-element-type ui-element))))
|#

