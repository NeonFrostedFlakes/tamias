(defmethod ui.mouse.click (ui-element (ui-type (eql :file-selector)))
  (setf (ui-fs-selector-active? ui-element) t)
  (let ((true-y (floor (/ (- mouse.ui-y (+ 2 (ui-y ui-element))) (+ char-height 2)))))
    (setf (ui-lister-current ui-element) true-y)))

(defmethod ui.kb-input (ui-element (ui-type (eql :file-selector)) (kb-input (eql :return)))
  (if (= (ui-fs-current ui-element) 0)
      (ui-fs.up-path ui-element)
      (let ((cur-child (elt (ui-fs-children ui-element) (ui-fs-current ui-element))))
	(if (uiop:directory-pathname-p cur-child)
	    (ui-fs.change-path ui-element cur-child)
	    (ui-fs.open-file ui-element);;open the file lol
	    ))))

(defmethod ui.kb-input (ui-element (ui-type (eql :file-selector)) (kb-input (eql :down)))
  (selection-cursor-down (ui-lister-current ui-element)
			 (1- (length (ui-fs-children ui-element)))
			 t)
  ;;(ui-lister-total-objs ui-element)
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :file-selector)) (kb-input (eql :up)))
  (selection-cursor-up (ui-lister-current ui-element)
		       (1- (length (ui-fs-children ui-element)))
		       t)
  )

(defmethod ui.kb-input (ui-element (ui-type (eql :file-selector)) (kb-input (eql :|.|)))
  (if (shift-t)
      (incf (ui-width ui-element) 16))
  )

