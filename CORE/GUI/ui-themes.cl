"THEMES YOU FUCKING IDIOT, THEMES"

(defvar ui.use-theme nil)

(defstruct theme
  name
  clear-color
  bg-color
  fg-color
  button-bg-color
  window-bg-color
  spin-box-color
  scroll-bar-color
  scroll-button-color
  font-color)

(defmacro define-ui-theme (theme &key clear-color bg-color fg-color
				   button-bg-color window-bg-color
				   spin-box-color scroll-bar-color scroll-button-color)
  `(defvar ,theme (make-theme  :name ',theme
			       :clear-color         ,clear-color
			       :bg-color	    ,bg-color		
			       :fg-color	    ,fg-color		
			       :button-bg-color	    ,button-bg-color	
			       :window-bg-color	    ,window-bg-color	
			       :spin-box-color	    ,spin-box-color	
			       :scroll-bar-color    ,scroll-bar-color	
			       :scroll-button-color ,scroll-button-color)))

(defmacro list-other-keys (&rest args &key x &allow-other-keys)
  `(print ',args))


(define-ui-theme default-ui-theme :clear-color #(0 0 0 255) :fg-color (tamias.colors:offset tamias.colors:+aqua+ -80) :bg-color tamias.colors:+aqua+)
  
(defmacro ui.set-theme (theme)
  `(setf ui.use-theme ',theme
	 render:font-color (or (theme-font-color ,theme) render:font-color)
	 tamias:render-clear-color (or (theme-clear-color ,theme) tamias:render-clear-color)))
  
