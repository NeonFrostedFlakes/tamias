(defun ui.render-scroll-bar-horizontal
    (scroll-bar x y w h &optional (color tamias.colors:+aqua+))
  (ui.render-bg scroll-bar
		x y w h)
  (ui.render-bg scroll-bar
		(+ x (scroll-offset scroll-bar)) y
		w h 0 0 color))

(defun ui.render-scroll-bar-vertical
    (scroll-bar x y w h &optional (color tamias.colors:+aqua+))
  (ui.render-bg scroll-bar
		x y w h)
  (ui.render-bg scroll-bar
		x (+ (scroll-offset scroll-bar) y)
		w h 0 0 color))


(defun ui.render-scroll-bar (scroll-bar &optional offset-x offset-y w-offset h-offset hover)
  (declare (ignorable hover))
  (if scroll-bar
      (let ((horizontal? (scroll-bar-horizontal? scroll-bar))
	    (left-or-top? (scroll-bar-placement scroll-bar)))
	(if horizontal?
	    (if left-or-top?
		;; "render horizontal at top"
		(ui.render-scroll-bar-horizontal
		 scroll-bar offset-x offset-y
		 w-offset 16)
		;; "render horizontal at bottom"
		(ui.render-scroll-bar-horizontal
		 scroll-bar offset-x
		 (- (+ offset-y h-offset)
		    (or (ui-height scroll-bar) 16))
		 w-offset 16))
	    (if left-or-top?
;;		"render vertical on left"
		(ui.render-scroll-bar-vertical
		 scroll-bar 
		 offset-x offset-y
		 16 h-offset)
;;    	        "render vertical on right"
		(ui.render-scroll-bar-vertical
		 scroll-bar
		 (- (+ offset-y w-offset)
		    (or (ui-width scroll-bar) 16))
		 offset-y
		 16 h-offset)))
	"Use the offset variables to determine where exactly the scroll-bar will be rendered."
	"orientation and placement are either nil or t. orientation mil is vertical, placement nil is right/bot. ori t is horizontal, plac t is left/top."
	)))
