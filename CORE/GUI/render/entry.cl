(defun render.text-cursor (text-struct str x y)
  (cursor-blink-check text-struct)
  (if (text-entry-column-limit text-struct)
      (let ((y-acc 0)
	    (x-acc 0)
	    (text-group (tamias.string:text-chunker str (text-entry-column-limit text-struct))))
	(loop for txt in text-group
	      do (if (> (text-entry-position text-struct)
			(+ y-acc (text-entry-column-limit text-struct)))
		     (incf y-acc (text-entry-column-limit text-struct))
		     (setf x-acc (- (text-entry-position text-struct)
				    y-acc))))
	(if (not (tamias-text-cursor-hide text-struct))
	    (render:box (+ x x-acc) (+ y y-acc)
			3 (cadr tamias.string:character-size))))
      (if (not (tamias-text-cursor-hide text-struct))
	  (let ((x-acc 0)
		(newline-positions '(0))
		(y-acc 0)
		(current-nl-pos 0))
	    ;;X-acc, x accumulator, pushes the cursor to the left
	    ;;y-acc, y accumulator, pushes it 'down'
	    ;;current-nl-pos is current newline position, it's used to figure out where the cursor is in relation
	    ;;   to the newlines
	    ;;newline-positions are used to help x-acc be configured correctly
	    (if (find #\newline str)
		(loop for n below (count #\newline str)
		      do (setf current-nl-pos (position #\newline str :start (1+ current-nl-pos)))
			 ;;if the cursor is before the next line, exit the loop
			 (if (< (text-entry-position text-struct)
				(1+ current-nl-pos))
			     (return x-acc))
			 (push current-nl-pos newline-positions)))
	    (if newline-positions
		(progn (setf y-acc (1- (length newline-positions)))
		       (let ((n 0))
			 (loop while newline-positions
			       do (setf n (pop newline-positions))
				  (if newline-positions
				      (incf x-acc (- n (car newline-positions))))))))
	    (if (> y-acc 0)
		(incf x-acc))
	    (render:box (- (+ x (* 16 (text-entry-position text-struct)))
			   (* 16 x-acc))
			(+ y (* y-acc 16))
			3 (cadr tamias.string:character-size))))))

(defvar entry-height-max nil)

(defmethod ui.render (ui-element (ui-type (eql :entry)) &optional hover (x-offset 0) (y-offset 0))
  (let ((x (+ x-offset (ui-base-x ui-element)))
	(y (+ y-offset (ui-base-y ui-element)))
	(w (ui-base-width ui-element))
	(h (ui-base-height ui-element))
	(str (text-entry-text (ui-element-entry ui-element)))
	(edited (text-entry-edited (ui-element-entry ui-element)))
	(text-struct (ui-element-entry ui-element)))
    (ui.render-bg ui-element)
    (when (eq (ui-id ui-element) hover)
	(render.cursor 4 36 (list 200 200 200 255))
	;;send message to mouse sub-system to switch to text cursor, that "I" thing
	)
    (if (eq current-text-context text-struct)
	(render.text-cursor text-struct str x y))
    ;;create a text buffer for the ui-element if text has been edited
    (if edited
	(progn (if (not (tamias-string-buffer (ui-label ui-element)))
		   (remove-buffer-on-quit ui-element))
	       (render:to-buffer str (tamias-string-buffer (ui-label ui-element)))))
    ;;render the text so that it's inside the entry element
    (let ((txt-dim (render:text-dimensions str)))
      (if (> h (cadr txt-dim))
	  (render:text-buffer (tamias-string-buffer (ui-label ui-element))
			      (list 0 0 w h) (list x y (- w (- w (car txt-dim)))
						   (- h (- h (cadr txt-dim)))))
	  (render:text-buffer (tamias-string-buffer (ui-label ui-element))
			      (list 0 0 w h) (list x y (- w (- w (car txt-dim)))
						   h))))
    ;;source-rect will be offset by scrollbars
    ;;(ui.render-text-chunker str x y w h)
    ))
