(defun ui.render-lister-bg (ui-lister &optional (x-offset 0) (y-offset 0))
  (let ((x x-offset)
	(y y-offset))
;;    (setf lister-rendered? t)
    (ui.render-bg ui-lister
		  x
		  y)))
#|
(defun ui.render-lister-objs (ui-lister x y)
  (let ((y-acc -16)
	(selector (ui-lister-current ui-lister))
	(c-s (ui-lister-child-objects ui-lister))
	(active? (ui-lister-selector-active? ui-lister)))
    (loop :for obj :in c-s
       :for n :below (length c-s)
       :do (if (and (eq selector n)
		   active?)
	      (render.hover-bg x (+ y (+ y-acc 16))
			       (ui-width ui-lister)
			       char-height
			       (tamias.colors:offset (ui-lister-selector-color ui-lister) 80)))
       ;;this next line is a little clever, assuming it works correctly
	 ;;y-acc is at -16 here at 0 loops. the incf will mutate y-acc to 0, resulting in y + 0 at 1 loops. 2 loops will be 16 and so on (this assumes monospaced fonts of 16x16 characters)
	 (ui.render-text (lister-obj-str obj) x (+ y (incf y-acc 16)))
	 )))

|#

(defun ui.render-lister-objs (ui-list-objs x y width selector active? color)
  (loop :for obj :in ui-list-objs
     :for n :below (length ui-list-objs)
     :for y-acc :below (* (length ui-list-objs) 16) by 16
     do (if (and (eq selector n)
		 active?)
	    (render.hover-bg x (+ y y-acc)
			     width
			     char-height
			     color))
       (ui.render-text (lister-obj-str obj)
		       x (+ y y-acc))))


(defmethod ui.render (ui-element (ui-type (eql :lister)) &optional hover (x-offset 0) (y-offset 0))
  (declare (ignore hover))
  (let ((x (+ x-offset (ui-base-x ui-element)))
	(y (+ y-offset (ui-base-y ui-element)))
	(ui-lst ui-element))
    (ui.render-lister-bg ui-element x y)
    (ui.render-lister-objs (ui-lister-children ui-lst) x y
			   (ui-width ui-lst) (ui-lister-current ui-lst)
			   (ui-lister-selector-active? ui-lst) (ui-lister-selector-color ui-lst))))
			   
