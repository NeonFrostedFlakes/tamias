(defmethod ui.render (ui-element (ui-type (eql :spin-box)) &optional hover (x-offset 0) (y-offset 0))
  ;;  (declare (ignore hover))
  (let ((x-max (+ x-offset (ui-base-x ui-element) (ui-base-width ui-element)))
	(y-max (+ y-offset (ui-base-y ui-element) (ui-base-height ui-element)))
	(str (write-to-string (tamias-variable-value (number-entry-value (ui-element-custom ui-element))))))
    (ui.render-bg ui-element
		  (+ x-offset (ui-base-x ui-element))
		  (+ y-offset (ui-base-y ui-element))
		  (ui-base-width ui-element)
		  (ui-base-height ui-element))
    (ui.render-bg ui-element (- x-max 32) (ui-base-y ui-element) 32 16)
    (ui.render-bg ui-element (- x-max 32) (ui-base-height ui-element) 32 32)
    (when (eq (ui-id ui-element) hover)
      (if (and (>= tamias:*mouse-x* (- x-max 32))
	       (<= tamias:*mouse-x* x-max))
	  (if (and (>= tamias:*mouse-y* (- y-max 16))
		   (<= tamias:*mouse-y* y-max))
	      (ui.render-hover ui-element 0 16
			       (- x-max 32) (ui-base-height ui-element) 32 16)
	      (ui.render-hover ui-element 0 0
			       (- x-max 32) (ui-base-y ui-element) 32 16))
	  (ui.render-hover ui-element
			   x-offset y-offset
			   (ui-base-x ui-element)
			   (ui-base-y ui-element)
			   (- (ui-base-width ui-element) 32)
			   (ui-base-height ui-element))))
    (ui.render-text str (ui-base-x ui-element) (- y-max 16);;tamias char size
		    (ui-base-width ui-element) (ui-base-height ui-element))
    (render:text "+1" (- x-max 32) (ui-base-y ui-element) :width 32 :height 16)
    (render:text "-1" (- x-max 32) (- y-max 16) :width 32 :height 16)))
