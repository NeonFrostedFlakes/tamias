(defun render.menu (ui-menu x-offset y-offset)
  (render:box x-offset y-offset
	      (ui-menu-width ui-menu) (* (length (ui-menu-ids ui-menu)) (cadr tamias.string:character-size))
	      (or (ui-menu-color ui-menu) tamias.colors:+cream+)))
(defun render.menu-item (ui x-offset y-offset &key width)
  (render:box x-offset y-offset
	      (or width (ui-menu-item-width ui))
	      (cadr tamias.string:character-size)
	      (or (ui-menu-item-color ui) tamias.colors:+dark-red+))
  (if (eq (get-current-hover) (ui-id ui))
      (render:box x-offset y-offset
		  (or width (ui-menu-item-width ui))
		  (cadr tamias.string:character-size)
		  (tamias.colors:offset tamias.colors:+dark-red+)))
  (if (not (tamias-string-buffer (ui-label ui)))
      (progn (push (ui-buff-func ui) quit-functions)
	     (render:to-buffer (tamias-string-text (ui-label ui))
			       (tamias-string-buffer (ui-label ui)))))
  (render:text-buffer (tamias-string-buffer (ui-label ui))
		      nil
		      (list x-offset y-offset))
;;      (render:text (tamias-string-text (ui-menu-item-label ui-element)) x-offset y-offset))
  (if (ui-menu-active-item ui)
      (let ((ui-menu (gethash (ui-menu-active-item ui) (ui-menu-item-children-ids ui))))
	(let ((current-x-offset (+ x-offset (ui-menu-item-width ui)))
	      (current-y-offset y-offset)
	      (ui-menu (ui-menu-active-item ui-menu)))
	  (render.menu ui-menu x-offset y-offset)
	  (loop for ui-item in (ui-menu-ids ui-menu)
		do (render.menu-item ui-item current-x-offset current-y-offset
				     :width (ui-menu-width ui-menu))
		   (incf current-y-offset (cadr tamias.string:character-size)))))))

(defmethod ui.render (ui-element (ui-type (eql :menu-item)) &optional hover (x-offset 0) (y-offset 0))
  (if hover
      (ui.render-hover ui-element x-offset y-offset))
  (render.menu-item ui-element x-offset y-offset)
  )
