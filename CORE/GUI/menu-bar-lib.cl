(defmacro menu-bar.init (state sub-state)
  `(progn (if (not (gethash ',state ui-managers))
	     (progn (setf (gethash ',state ui-managers) (make-hash-table))
		    (setf (gethash ',sub-state (gethash ',state ui-managers)) (make-ui-manager))))
	 (setf (get-menu-bar ',state ',sub-state) (make-ui-menu-bar))
	 (push 'menu-bar (ui-manager-collection-ids (get-ui-manager ',state ',sub-state)))))

(defun ui.menu-child (menu child-sym)
  (gethash child-sym (ui-children menu)))

(defun ui.menu-bar-child (menu-bar child-sym)
  (ui.menu-child menu-bar child-sym))

(defun ui.add-mb-child (state sub-state ui-element)
  (manager.init? state sub-state)
  (setf (gethash (ui-id ui-element) (ui-manager-collection (gethash sub-state (gethash state ui-managers))))
	ui-element))

(defun add-menu-bar-item (state sub-state item &key label action color)
  "Add's a 'top level' menu bar item, like 'File' or 'Edit'. Helper function"
    (let ((label? label)
	  ;;(mbar-item (get-menu-bar-item state sub-state item))
	  )
      (if (not label?)
	  (progn (setf label (string-downcase (write-to-string item)))
		 (setf label (concatenate 'string (string-upcase (subseq label 0 1)) (subseq label 1)))))
      (eval `(push-quit (render:remove-buffer (tamias-string-buffer (ui-menu-item-label (get-menu-bar-item ',state ',sub-state ',item))))))
      (setf (get-menu-bar-item state sub-state item)
	    (make-ui-menu-item :width (* (length label)
					 (car tamias.string:character-size))
			       :height (cadr tamias.string:character-size)
			       :label (make-tamias-string :text label)
			       :action action :color color :buff-func (pop quit-functions)
			       ))
      (ui.add-mb-child state sub-state (get-menu-bar-item state sub-state item))
      (aux:push-to-end item (ui-container-children-ids (get-menu-bar state sub-state)))))

(defmacro menu-bar.add-item (state sub-state item &key label action color)
  "Primary macro to add an item to the menu-bar of state and sub-state.
Example: (menu-bar.add-item 'blender 'sculpt 'tool (:color '(127 127 127 255))"
  `(add-menu-bar-item ',state ',sub-state ',item :label ,label :action ',action :color ,color)
  )

(defun menu-bar-item.add-item (item item-parent state sub-state &key label action color)
  "Helper function, add's an item to a menu-bar item (or an item of a menu-bar item). Ex: blender -> menu-bar -> File -> import -> fbx"  
  (let ((label? label)
	(menu-item nil)) 
    (if (not label?)
	(progn (setf label (string-downcase (write-to-string item)))
	       (setf label (concatenate 'string (string-upcase (subseq label 0 1)) (subseq label 1)))))
    (eval `(push-quit (render:remove-buffer
		       (tamias-string-buffer
			(ui-menu-item-label (get-menu-item ',item (get-menu-bar-item ',state ',sub-state ',item-parent)))))))
  
    (setf menu-item (make-ui-menu-item :width (* (length label) (car tamias.string:character-size))
				       :height (cadr tamias.string:character-size)
				       :label (make-tamias-string :text label)
				       :action action :color color
				       :buff-func (pop quit-functions)))
    (setf (get-menu-item item
			 (get-menu-bar-item state sub-state item-parent))
	  menu-item)
    (if (> (ui-menu-item-width (get-menu-item
				item
				(get-menu-bar-item state sub-state item-parent)))
	   (ui-menu-widest (get-menu-bar-item state sub-state item-parent)))
	(setf (ui-menu-widest
	       (get-menu-bar-item state sub-state item-parent))

	      (ui-menu-item-width
	       (get-menu-item
		item
		(get-menu-bar-item state sub-state item-parent)))))
    (ui.add-mb-child state sub-state
		     (get-menu-item item
				    (get-menu-bar-item state sub-state item-parent)))
    (aux:push-to-end item (ui-menu-children-ids (get-menu-bar-item state sub-state item-parent)))))

(defmacro menu-bar.item.add-item (item item-parent state sub-state &key label action color)
  "add's an item to a menu-bar item (or an item of a menu-bar item). 
Example: (menu-bar.item.add-item import file blender sculpt :label \"import\")"
  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))

(defmacro menu-bar.item.add-child (item item-parent state sub-state &key label action color)
  "add's an item to a menu-bar item (or an item of a menu-bar item). 
Example: (menu-bar.item.add-item import file blender sculpt :label \"import\")"
  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))

(defmacro menu-bar.child.add-item (item item-parent state sub-state &key label action color)
  `(menu-bar-item.add-item ',item ',item-parent ',state ',sub-state :label ,label :action ',action :color ,color))

(defmacro menu-bar.add-items (state sub-state &rest items)
  `(loop for item in (reverse ',items)
	 do (menu-bar.add-item ',state ',sub-state item)))

