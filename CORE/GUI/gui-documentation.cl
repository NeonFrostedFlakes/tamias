(document ui-manager
"This is gonna be great to explain.
So, to start with: every single ui-element that is made is shoved into a ui-manager
Quick example: (add-state-ui-element battle select-magic (make-ui-frame))
               This will put the ui-frame into the ui-manager for battle and select-magic
               assuming that ui-frame has id of 0, do 
               (get-ui-element 0 battle select-magic) to get the ui-frame
               the current-ui-id is completely global, however it may be attached to each ui-manager instance

current-ui-id is initially set to -1. This is to facilitate the id to be set to 0 for the first ui-element. (when make-ui is called, it automatically increases the current-ui-id and sets the ui-element's id to that new value)

Under Tamias, each ui-manager manages their elements through ui-manager-collection, a hash table.")

(document def-ui-alias
	  "def-ui-alias is used to facilitate the creation of *macros* that can be used with SETF.
The idea is very simple really. I might feel like a function with a different name will be better suited for a particular accessor, but it won't be understood in a broader audience, or it's connection might not be as clear, even though it does the same thing.
Example: ui-manager-ids. While ui-manager-ids is a macro that points to ui-manager-collection-ids, its connection to ui-manager-collection isn't as clear. However, it's easily understood that ui-manager-ids are all the ids used with the ui-manager")

(document ui-pack-elements
	  "ui-pack-elements 'packs' elements into a ui-container (i.e. ui-window, ui-dock, ui-frame)." ""
		   "First we get the packing offset for the container. The function that (ui-pack-offset-x instance) is wrapped around is (aref (ui-container-pack-offset instance) 0)." ""
		   "Next, we set the x and y of the current ui-element to ui-pack-offset-x and y of the container instance." 
		   "" "After that, we increase the x offset with the width of the ui-element. Now, we check if the x offset is larger than the containers width OR if the x-offset plus the width of the next ui-element would be larger than the containers width. If so, then we set the offset-x to 0 and the offset-y to the height of the first ui-element in the 'row'." 
		   "" "We repeat this process until all the ui-elements have been processed.")

(document ui-frame-child-xy-explanation
"Adding an element to a ui-frame does not bolt down it's x/y position. Rather, the x/y postion is used in conjunction with the x/y position of the ui-frame
the x/y position of an element in a frame is in relation to that frame
So, a simple example: Frame1->Frame2->Element1 . El-1 would be augmented by F2's x/y position. F2 would be augmented by F1.")


(document ui-window "Windows are mini-windows in an application. They are movable, have scroll-bars, have title-bars and so on. Can be closed, functionality coming soon ^TM.
They are a sub-class of the ui-container class (so that ui-container functions can be used with them, like ui-container-children")
