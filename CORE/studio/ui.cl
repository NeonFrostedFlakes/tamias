(defun new-file ()
  )

(defun open-file-dialog ()
  (set-sub-state 'open-file)
  )

(defun close-file-dialog ()
  (set-sub-state 'top)
  )

(def-render (studio 'open-file)
    (render.gui studio 'top)
  (render.gui studio 'open-file))

(menu-bar.init studio 'top)
(menu-bar.add-items studio top file edit view)

(menu-bar.item.add-item exit file
			studio top :action (quit-game))
(menu-bar.item.add-item open file
			studio top :action (open-file-dialog))
(menu-bar.item.add-item new file
			studio top :action (new-file))

(menu-bar.item.add-item undo edit
			studio top :action (print 'undone))

(ui.add-element 'studio 'top (make-ui-frame :x 0 :y 0 :width (round (/ tamias:screen-width 4)) :height (round (/ tamias:screen-height 4))))

(ui.add-element 'studio 'open-file
		(make-ui-window :x (round (/ tamias:screen-width 16)) :y (round (/ tamias:screen-width 16))
				:width (round (* 13 (/ tamias:screen-width 16))) :height (round (* 13 (/ tamias:screen-height 16)))
				:color tamias.colors:+aqua+))

(define-tamias-key close-file-dialog-key :q)
(add-action-to-key close-file-dialog-key studio 'open-file (close-file-dialog))
;;(add-state-ui-element 'studio 'top (make-ui-button :x 200 :x-equation 200 :y 200 :y-equation 200 :color (list 0 90 0 255)))
;;(add-state-ui-element 'studio 'top (make-ui-text :x 50 :x-equation 50 :y 52 :y-equation 20 :width 400 :width-equation 400 :height 24 :height-equation 24))

