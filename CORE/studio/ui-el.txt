Timer maker

+-----------------------+
|Timer Definer		|
|_______________________|
|Named	      [________]|  Text-entry
|Incrementor    [___]   |  number-entry
|Ends	        [___]   |  number-entry
|  In-seconds?   [_]    |  checkbox
|Resets?         [_]    |  checkbox
+-----------------------+
