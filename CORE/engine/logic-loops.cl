(defconstant tamias-up-keyword :up)
(defvar post-processing nil)
(defvar tmp-proc nil)

(defun exit-tamias? ()
  )

(defun post-processing ()
  (loop for proc in post-processing
	do (eval proc)))

;;(push '(render:box 0 0 tamias:screen-width tamias:screen-height #(0 255 0 70)) post-processing)

(defun tmp-proc ()
  (loop while tmp-proc
	do (eval (pop tmp-proc))))

(defun handle-world ()
  :world)

(defun process-loop ()
  (tamias:logic tamias:state (states:state-sub-state tamias:state))
  (when (states:state-world states:state)
    (handle-world))
  (when tamias:to-state
    (setf tamias:state tamias:to-state
	  tamias:to-state nil)) ;;this makes it so that you can switch states from inside a state. (problems came about with the splash.cl test)
  )
#|
(defun pause-game ()
  (set-sub-state paused)
  (pause-music))

(defun resume-game ()
  (set-sub-state level)
  (resume-music))
|#

(defun game-loop ()
  (test-music) ;;process music and sounds if necessary
  (process-loop)
  (render-state) ;; DO NOT CHANGE THIS TO states:... !!! my dumbass made one in cl-user AND states
  ;;>.........< Fuck me god damn it
  (when tamias:changing-state
    (process-changing-state)))
  

(defvar r.cursor? t)

(defun no-gui-process-kb ()
  (symbol-macrolet ((kb-key (elt tamias-current-key 0))
		    (key-state (elt tamias-current-key 1))
		    (num-frames-held (elt tamias-current-key 2))
		    (is-being-held? (elt tamias-current-key 3))
		    (is-processed? (elt tamias-current-key 4)))
    (when kb-key
      (if is-being-held?
	  (when (> (mod num-frames-held 3) 0)
	    (setf is-being-held? nil
		  is-processed? t))
;;	  (progn (tamias.gui.key-press)
	  (setf is-being-held? t
		is-processed? nil))
      (when (not is-processed?)
	(if (find-if #'alpha-char-p (write-to-string kb-key))
	    (tamias-key-handler kb-key key-state tamias:state (states:state-sub-state tamias:state) (ctrl-t) (alt-t) (shift-t))
	    (tamias-key-handler kb-key key-state tamias:state (states:state-sub-state tamias:state) (ctrl-t) (alt-t))))
      (when (eql key-state tamias-up-keyword)
	(setf kb-key nil
	      key-state nil
	      is-processed? nil)))))

(defun no-gui-process-mouse ()
    (let ((m-button (elt *t-mouse* 0)))
;;    (tamias.gui.mouse-move)
      (when m-button
	(incf mouse-timer)
	(let ((m-b-state (elt *t-mouse* 1))
	      (m-moving (elt *t-mouse* 2)))
	  ;;(when (eq (mod mouse-timer 2) 0)
	  ;;  (tamias.gui.click))
	  (when (> mouse-timer 1)
	    (tamias:mouse-input m-button m-b-state
				tamias:state (states:state-sub-state tamias:state)
				m-moving))
	  (when (and (> mouse-timer 30)
		     (not m-moving))
	    (setf mouse-timer 0))
	  (when (eq m-b-state tamias-up-keyword)
	    (setf (elt *t-mouse* 0) nil))))))




(defun gui-process-kb (tamias-current-key)
  (symbol-macrolet ((kb-key (elt tamias-current-key 0))
		    (key-state (elt tamias-current-key 1)))
    (when kb-key
      (if (not (tamias.gui.key-press kb-key))
	  (states:tamias-key-handler kb-key key-state tamias:state (states:state-sub-state tamias:state) (ctrl-t) (alt-t) (shift-t))
	  ))))
;;	      (states:tamias-key-handler kb-key key-state tamias:state (states:state-sub-state tamias:state) (ctrl-t) (alt-t)))))))



(defun gui-process-mouse ()
  (let ((m-button (elt *t-mouse* 0)))
    (tamias.gui.mouse-move)
    (when (not m-button)
      (let ((m-b-state (elt *t-mouse* 1))
	    (m-moving (elt *t-mouse* 2)))
      	(when (> mouse-timer 1)
	  (tamias:mouse-input m-button m-b-state
			      tamias:state (states:state-sub-state tamias:state)
			      m-moving))))
    (when m-button
      (incf mouse-timer)
      (let ((m-b-state (elt *t-mouse* 1))
	    (m-moving (elt *t-mouse* 2)))
	(when (eq (mod mouse-timer 2) 0)
	  (ignore-errors (tamias.gui.click)))
	;;(when (> mouse-timer some-arbitray.number!!!!)
	;;  (tamias.gui.hold))
	(when (> mouse-timer 1)
	  (tamias:mouse-input m-button m-b-state
				    tamias:state (states:state-sub-state tamias:state)
				    m-moving))
	(when (and (> mouse-timer 30)
		   (not m-moving))
	  (setf mouse-timer 0))
	(when (eq m-b-state tamias-up-keyword)
	  (setf (elt *t-mouse* 0) nil))))))


(defun primary-loop ()
  (when tamias:current-track
    (timer:increment (track-timer tamias:current-track))
    (timer:end? (track-timer tamias:current-track)))
;;  (render:text (write-to-string tamias-key-queue) 20 20)
  (if (states:state-using-gui? tamias:state)
      (progn (loop :for kbk :in tamias-key-queue
		   :do (gui-process-kb kbk))
	     (setf tamias-key-queue nil)
	     (gui-process-mouse))
      (progn   (no-gui-process-kb)
	       (no-gui-process-mouse)))
  (states:process-active-inputs)
  (when current-text-context
    (when tamias-text-insert
      (handle-text-input)))
  (game-loop)
  (post-processing)
  (when r.cursor?
    (render.cursor))
  (when gui-key-pressed?
    (incf gui-key-timer)
    (when (>= gui-key-timer 3)
      (setf gui-key-pressed? nil
	    gui-key-timer 0)))
  (when tamias:target-resolution-changed
    (tamias:set-scale-factor (elt tamias:target-resolution 0)
			     (elt tamias:target-resolution 1)))
  (exit-tamias?))

