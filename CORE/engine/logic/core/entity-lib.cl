(defpackage tamias.entities
  (:use :cl)
  (:nicknames :t.e))
(in-package :tamias.entities)

(defmacro with-hash (hash-table &body body)
  `(flet ((set-hash (&rest arg-pairs)
		    (loop :for (key value) :on arg-pairs :by #'cddr
			  :do (setf (gethash key ,hash-table) value)))
	  (get-hash (key)
	    (gethash key ,hash-table)))
     ,@body))

(defstruct t-object
  (type :object :type keyword) ;;is a keyword
  layer-id
  (position (vector 0 0 0) :type vector)
  (init-position (vector 0 0 0) :type vector) ;;not sure if It'll actaully be necessary
  (velocity (vector 0 0 0) :type vector)
  (width 16 :type integer)
  (height 16 :type integer)
  (current-cell 0)
  sprite-sheet
  texture
;;  (vector (make-vector-3d)) ;;movement-vector
;;  (acceleration (make-vector-3d :z 0)) ;;accelerate vector
  (state :init)
  (friction 1)
  (mass 1)
  (alive? t)
  bounding-boxes
  (symbol (gensym "TE-"))
  physics
  off-screen)

(defmacro t-object-init-x (t-object)
  `(elt (t-object-init-position ,t-object) 0))
(defmacro t-object-init-y (t-object)
  `(elt (t-object-init-position ,t-object) 1))
(defmacro t-object-init-z (t-object)
  `(elt (t-object-init-position ,t-object) 2))

(defmacro t-object-x (t-object)
  `(elt (t-object-position ,t-object) 0))
(defmacro t-object-y (t-object)
  `(elt (t-object-position ,t-object) 1))
(defmacro t-object-z (t-object)
  `(elt (t-object-position ,t-object) 2))

(defmacro t-object-velocity-x (t-object)
  `(elt (t-object-velocity ,t-object) 0))
(defmacro t-object-velocity-y (t-object)
  `(elt (t-object-velocity ,t-object) 1))
(defmacro t-object-velocity-z (t-object)
  `(elt (t-object-velocity ,t-object) 2))


(export
 '(make-t-object
   t-object
   t-object-off-screen
   t-object-velocity-x
   t-object-velocity-y
   t-object-velocity-z 
   t-object-velocity
   t-object-state
   t-object-type
   t-object-layer-id
   t-object-position
   t-object-init-position
   t-object-width
   t-object-height
   t-object-current-cell
   t-object-sprite-sheet
   t-object-texture
   t-object-friction
   t-object-mass
   t-object-alive?
   t-object-bounding-boxes
   t-object-symbol
   t-object-init-y
   t-object-init-x
   t-object-init-z
   t-object-x
   t-object-y 
   t-object-z
   t-object-p
   t-object-physics))
	
(defstruct attack
  damage
  range
  num-targets
  collision-duration ;;in frames, but can be declared as being in seconds (attack.duration.collision 3 :seconds)
  render-duration ;;in frames, same as coll-dur. if nil, will use collision-duration, if coll-dir nil, sets both to 0, inverse is true for coll-dur (coll-sur nil, use ren-dur, if nil, 0 for both)
  )

(export
 '(make-attack
   attack-damage
   attack-range
   attack-p
   attack-num-targets
   attack-collision-duration))

(defstruct (entity (:include t-object
		    (type :entity)))
  ;;input-table
  direction
  behaviors
  (name "TE");;unless supplied, use entity symbol
  health
  (attacks (list (make-attack))))


;;states examples: :init :idle :attacking :defending


(export
 '(entity-p
   entity-velocity
   entity-velocity-x
   entity-velocity-y
   entity-velocity-z
   entity-physics
   entity-position
   entity-z
   entity-x
   entity-y
   entity-direction
   entity-layer-id
   entity-behaviors
   entity-type
   entity-name
   entity-state
   entity-init-position
   entity-width
   entity-height
   entity-current-cell
   entity-sprite-sheet
   entity-texture
   entity-friction
   entity-mass
   entity-alive?
   entity-bounding-boxes
   entity-symbol
   entity-type
   entity-attack
   entity-health
   make-entity
   entity
   entity-off-screen))

(defmacro entity-velocity-x (entity)
  `(elt (entity-velocity ,entity) 0))
(defmacro entity-velocity-y (entity)
  `(elt (entity-velocity ,entity) 1))
(defmacro entity-velocity-z (entity)
  `(elt (entity-velocity ,entity) 2))

(defmacro entity-x (entity)
  `(elt (entity-position ,entity) 0))
(defmacro entity-y (entity)
  `(elt (entity-position ,entity) 1))
(defmacro entity-z (entity)
  `(elt (entity-position ,entity) 2))

(defun entity.define (struct-name &rest args)
  ;;entity.define is 
  ;;where's the entity name/symbol?
  ;;FUCK
  ;;Ah...it's the "struct-name"
  ;;Very insightful Neon
  (let ((entity-value-table (make-hash-table))
	(tmp-args (car args))
	(width 16)
	(height 16)
	(sprite-sheet nil)
	(friction 1)
	(mass 1)
	(bounding-boxes (quote (vector 0 0 16 16)))
	(inherit-slots nil)
	(custom-slots nil)
	(attacks nil))
    (with-hash entity-value-table
      (set-hash :width width
		:height height
		:sprite-sheet sprite-sheet
		:friction friction
		:mass mass
		:bounding-boxes bounding-boxes))
    ;;    (print (gethash :bounding-boxes entity-value-table))
    (loop for n below (length tmp-args)
	  do (let ((arg (elt tmp-args n)))
	       (if (listp arg)
		   (push arg custom-slots)
		   (if (keywordp arg)
		       (progn (setf (gethash arg entity-value-table)
				    (elt tmp-args (1+ (position arg tmp-args))))
			      (push arg inherit-slots)
			      (incf n))
		       (if (symbolp arg)
			   (push arg custom-slots))))))

    ;;;;;;;;;;;;;;;;;;;
    ;;The Code below uses my own helper macro "with-hash"
    ;;   get-hash and set-hash immediately assume that you are talking about
    ;;   the hash-table referenced after "with-hash"
    ;;   Here, get- and set- assume the "entity-value-table", declared above
    ;;;;;;;;;;;;;;;;;;;
    
    (with-hash entity-value-table
      (let ((slots-str "")
	    (entity-str "")
	    (inherit-from-str "tamias.entities:entity")
	    (inherit-slots-str "")
	    (type-str "")
	    (struct-name-str (write-to-string struct-name)))
	(loop :for slot :in custom-slots
	      :do (setf slots-str (concatenate 'string
					      slots-str
					      (write-to-string slot)
					      " ")))
	(loop :for slot :in (remove :include inherit-slots)
	      :do (setf inherit-slots-str (concatenate 'string
						       inherit-slots-str
						       "("
						       (remove #\: (write-to-string slot))
						       " "
						       (write-to-string (get-hash slot))
						       ") ")))
	;;	(print slots-str)
	(if (find :type inherit-slots)
	    (setf type-str "")
	    (setf type-str (concatenate 'string "(type " (concatenate 'string ":" struct-name-str) ")")))
	(if (get-hash :include)
	    (setf inherit-from-str (write-to-string (get-hash :include))))
	(setf entity-str (concatenate 'string
				      "(defstruct (" struct-name-str
				      "(:include " inherit-from-str
				      type-str
				      " " inherit-slots-str
				      " (name \"" struct-name-str "\")" 
				      "))"
				      slots-str ")"))
	(eval (read (make-string-input-stream 
		     entity-str)))
	;;not sure if I should add create-"struct-name"
	;;eh, fuck it, might as well
	(let ((create-string (concatenate
			      'string
			      "(setf (fdefinition 'create-"
			      struct-name-str ")"
			      " #'make-" struct-name-str ")")))
	  (eval (read (make-string-input-stream create-string))))
	
	))))
					  
(defmacro define-entity (entity-symbol &rest args)
  `(entity.define ',entity-symbol ',args))

(export 'define-entity)


(defmacro acceleration-x (object)
  `(t-object-acceleration ,object))
(defmacro acceleration-y (object)
  `(t-object-acceleration ,object))
(defmacro acceleration-z (object)
  `(t-object-acceleration ,object))

(export
 '(acceleration-x
   acceleration-y
   acceleration-z ))

;;(let ((pack (find-package :tamias.entities)))
;;  (do-all-symbols (sym pack) (when (eql (symbol-package sym) pack) (export sym))))

