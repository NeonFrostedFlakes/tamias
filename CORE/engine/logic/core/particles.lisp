#|
Uses vectors to move from one location to the next

|#
(defstruct particle
  vector
  dimensions
  (current 0)
  (lifetime 0)
  color)

#|
On 'z'
it creates a new particle
this particle has no name, it is part of a particles list
|#

(defmacro draw-particle (rect color)
  `(render:rectangle ,rect ,color t))
(defmacro create-particle ((vector (x y width height) lifetime color) particles)
  `(push (make-particle :vector ,vector :dimensions (vector ,x ,y ,width ,height)
			:lifetime (floor (* ,lifetime tamias:fps)) :color ,color) ,particles))
(defmacro decay-particle (particle)
  `(progn
     (incf (particle-current ,particle))
     (setf (elt (particle-color ,particle) 3) (floor (- 255
							(* 255 (/ (particle-current ,particle)
								  (particle-lifetime ,particle))))))
     (incf (elt (particle-dimensions ,particle) 0) (vec3d:x (particle-vector ,particle)))
     (incf (elt (particle-dimensions ,particle) 1) (vec3d:y (particle-vector ,particle)))))
#|
(defmacro push-particle (particle particles)
  `(if ,particles
       (aux:push-to-end ,particle ,particles)
       (push ,particle ,particles)))

(defun check-particles (particles) ;;list of particles
  (loop for particle in particles
	do (when (>= (particle-current particle) (particle-lifetime particle))
	     (setf (nth (position particle particles) particles) nil)))
  (setf particles (remove nil particles)))
|#

(defun particle-effect (particle)
  (when particle
    (let ((color (particle-color particle))
	  (dimensions (particle-dimensions particle))
	  (current (particle-current particle))
	  (lifetime (particle-lifetime particle)))
      (when (< current lifetime)
	(draw-particle dimensions color)))))

(defun render-particles (particles-list)
  (loop :for particle :in particles-list
	:do (particle-effect particle)))

(defmacro process-particles (particles-list)
  `(progn
     (loop :for particle :in ,particles-list
	   :do ;;(particle-effect particle)
	       (decay-particle particle)
	       (when (>= (particle-current particle) (particle-lifetime particle))
		 (setf ,particles-list (remove particle ,particles-list))))
     (remove-nil ,particles-list)))
