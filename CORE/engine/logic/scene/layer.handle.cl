(defmacro add-spawn-point ( layer &optional ( x 0 ) ( y 0 )
				 randomized? ( randomized-area '( 10 10 20 20 ))
				 timer   trigger   inhibitor   max-at-frame  (max-generated 1)
				 entity-type )
  `(let (( spawn-point ( make-spawn-point
			:spawn-func ( concatenate 'string "(make-"
						  ( subseq ( write-to-string ,entity-type ) 1 ))
			:x ,x :y ,y :randomized? ,randomized?
			:randomized-area ,randomized-area :timer ,timer
			:trigger ,trigger :inhibitor inhibitor :max-at-frame ,max-at-frame
			:max-generated ,max-generated :entity-type ,entity-type )))
    (push spawn-point ( layer-spawn-points ,layer ))))

(defun handle-layer ( layer ) 
  ( render:image ( layer-bg layer ) 0 0 )
  ( render.tile-map ( layer-tile-map layer ))
  'spawn-points ;;A list/array of spawn points, check the spawn stuff for spawning stuff, sad at time of writing sorry :(
  'UI? ;;render UI. Going to create a secondary UI system, one thats for Game UI only, G-UI is 'non-editable', I think (G-UI is game-UI, not GUI), may not do secondary, don't become attached to anything :(
  ;;Check if the layer
  ;;apply world logic to objects then render them
  (loop :for object :in ( layer-objects layer )
	:do (let (( t-object ( gethash object ( scene-objects scene ))))	     
	     (if ( entity-p t-object )
		 (let (( entity t-object ))
		   ( entity.handle entity entity.type ))
		 ( t-object.handle t-object t-object.type ))
	     ;;somewhere around here, we check to see if the layer will change the objects layer-id
	     ;;not sure how I'll do that exactly
	   )))

(defmacro add-entity-to-layer ( entity-sym layer )
  `(push ,entity-sym ( layer-objects-ids ,layer )))
;;entity-sym is incrementally listed as TE-n
;;part of me wants to use the entity name rather than just TE
;;located in one of the library files
;;line 32 (as of Dec 1 2023) of file CORE/engine/logic/core/entity-lib.cl

(defun remove-entity-from-layer ( entity-sym layer )
  (setf ( elt ( layer-objects-ids layer )
	      ( position entity-sym ( layer-objects-ids layer )))
	nil ))

(defun send-entity-to-layer ( entity cur-layer new-layer )
  (add-entity-to-layer ( tamias.entities:t-object-symbol entity ) new-layer )
  (remove-entity-from-layer ( tamias.entities:t-object-symbol entity ) cur-layer ))

#|
(defun gen-entity (layer)
  ;;placeholder code
  ;;On generation, (gensym (combine-strings (entity-name entity) "-")
  ;;This will allow for easier debugging than a simple TE-280
  ;;e.g. Minotaur-001 caused the game to halt. On another test Minotaur-004 also caused the game to halt
  ;;You figure out it's because an animation was referenced improperly
  ;;VS. TE-280 and TE-282 cause the game to halt at different points
  ;;Hours later, you figure out the bat's AI is looping for some ungodly reason
  )
|#
