#|
(defstruct bg-layer
  image)

(defstruct entity-layer
  (entity-groups (make-hash-table))
  spawn-points;;A list of spawn points
  )

(defstruct ui-layer
  UI)

(defstruct object-layer
  objects)
|#


(defstruct camera
  (x 0) (y 0) ;;more for a platformer, or an RPG that doesn't strictly move by tiles
  (tile-x -1) (tile-y -1) ;;easily better for RPGs (I think)
  (width-px tamias:screen-width) (height-px tamias:screen-height)
  (zoom-factor 1))

(defstruct spawn-point
  (x 0) (y 0) 
  randomized?    (randomized-area '(10 10 20 20)) ;;r-area: '(w-min h-min w-max h-max)
  timer   trigger   inhibitor    max-at-frame max-generated
  spawn-func ;;is a string, "(make-[ENTITY-TYPE])"
  entity-type ;;is a keyword
  )
;;randomized will be either nil or #(x-mod y-mod w-mod h-mod)

(defstruct collision-map ;;tile-based
  file  ;;
  (data (make-array '(1 1) :initial-element -1)));;should -1 be no impedence? or should that be 0?
;;It could make things more interesting by having 1 be slowed down, 2 turns you around, etc.
;;this does make things a bit more difficult because of ramps
;;Good god, I have to program ramps in? ...Fuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu
;;I wonder how you would...augment...hmmmm
;;So, I'm going to have make a separate txt file for some ideas

(defstruct tile-map
  file   (sheet (sprite:make-sheet)) ;;sheet is for gfx
  (data (make-array '(1 1)
		    :initial-element -1)))
;;load tile-map, data = read tile-map file
;;or use a loop read-line (for the 'column' intro) and then
;;       loop read integer from line, (aref y x data) (incf x) . outer: (incf y)

(defstruct layer
  bg  tile-map  objects-ids
  spawn-points;;A list/array of spawn points
  UI? ;;If UI?, then use the GUI manager subsystem (possibly adding in world->A->S->layer support is a bad idea tbh. Well, maybe not.)
  size ;;width and height
  >x >y ;;used to send entities to different layers. It's a way around incorrect rendering. And since a player will inherently be an entity, it means you don't need to write special code for a player (albeit it doesn't prevent one from doing so)
  visible 
  (id (incf layer-id)))

(defstruct portal
  x y width height
  player-pos-new
  to-scene ;;is a scene-id
  active?)

(defstruct scene
  (objects (make-hash-table :size 60))
  (camera (make-camera)) ;;used to determine where the tile-map is being rendered from
  (layers (make-hash-table))
  collision-map ;;need to either add a seamless transition to another scene, or modify how scenes and layers work
  (state :idle) ;; as opposed to :cutscene, :init, :fade, etc.
  layer-ids
  portals ;;this may become a quad-tree in itself, just one level deep
  (id (incf scene-id)))


(defvar current-world nil);;current-x are objects or symbol macros lol
