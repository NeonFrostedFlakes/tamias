"Spawn points:
spawn points can be set to a 'randomized' or 'absolute' state
    randomized will be for spawning entities within an area, who's size can be controlled by the programmer
    absolute is for spawning an entity at a specific spot

Let's say, (generate boar boar-spawn-1)

boar-spawn-1 has an absolute spot, say #(600 600 2)
so, boar(s) will be spawned on that exact spot, X: 600, Y: 600, layer 2 (of whichever scene graph the generator is attached to)

(generate boar boar-spawn-6 gen-amount max-amount)

gen-amount is how many boars to be generated, with max-amount the number of times an entity can be generated at spawn-point

20 after b-s-6 indicates number of boars to be spawned
boar-spawn-6 has a randomized state centered on #(200 200 2)
   the randomized spawn area has an x-modifier of 40, width-mod of 40, y of 40, h of 40
   The total possible locations are [(160, 160), (240 240)]
   Possible spawn spots: [160, 180] [237, 180]
"

(defmacro get-layer ( scene layer-id )
  `(gethash ,layer-id ( scene-layers ,scene )))
(defmacro get-scene ( area scene-id )
  `(gethash ,scene-id ( area-scenes ,area )))
(defmacro get-area ( world area-id )
  `(gethash ,area-id ( world-areas ,world )))
(defmacro get-camera ( &optional ( scene current-scene ))
  `(scene-camera ,scene ))


(defun switch-scene ( scene )
  (setf current-scene
	    ( gethash ( scene-id scene ) ( area-scenes current-area ))
	( area-scene current-area )
	    ( scene-id scene )))


(defun handle-world ()
  'world
  )


(defmacro spawn-entity ( entity-class &optional ( x 0 ) ( y 0 ) ( z 0 ))
  `(let (( entity (if ,entity-class
		     ( eval ( read-from-string ,entity-class ))
		     ( make-entity ))))
     (setf ( entity-position entity ) ( vector ,x ,y ,z ))
     entity ))


(defun generate-entity ( scene layer entity-spawn-point )
  "Assume (generate room-x layer-n boar-point-1)"
  (let* (( layer-id ( layer-id layer ))
	 ( rand-area ( spawn-point-randomized-area entity-spawn-point ))
	 ( spawn-x-random (if ( spawn-point-randomized? entity-spawn-point )
			     (+ ( elt 0 rand-area )
				( random ( elt 2 rand-area )))
			     0 ))
	 ( spawn-y-random (if ( spawn-point-randomized? entity-spawn-point )
			     (+ ( elt 1 rand-area )
				( random ( elt 3 rand-area )))
			     0 ))
	 ( entity ( spawn-entity ( spawn-point-entity-spawn-func spawn-point )
			       (+ spawn-x-random ( spawn-point-x spawn-point ))
			       (+ spawn-y-random ( spawn-point-y spawn-point ))))
	 ( entity-sym ( tamias.entities:t-object-symbol entity )))
    (push entity-sym
	  ( layer-objects-ids layer ))
    (setf ( gethash entity-sym
		   ( scene-objects scene ))
	  entity )))
#|
(defun test-spawn ()
  "The spawn accumulator increases by a certain number (1 or >1) at the start of every player (note: not `creatures`) turn.
spawn-accumulator is increased before test-spawn is called."
  (if (> spawn-accumulator default-spawn-rate) ;;;;TODO: affect by the level **and** areas spawn rate
      (progn (spawn-creature (nth (random (length (area-creatures (level-areas (player-level user)))))
				  (area-creatures (level-areas (player-level user))))
			     (list (1+ (random (- (cadr (array-dimensions main-map)) 2)))
				   (1+ (random (- (car (array-dimensions main-map)) 2)))))
	     (push-message "You feel as though something was born nearby.")
	     (setf spawn-accumulator 0))
      (incf spawn-accumulator 1)))
|#

"(spawn-entity :boar 10 10 0)"

(defmacro add-layer-object ( layer data )
  `(push ( tamias.entities:t-object-symbol ,data )
	 ( layer-objects ,layer )))

(defmacro get-scene-entity ( entity scene )
  `(gethash ( tamias.entities:t-object-symbol ,entity ) ( scene-objects ,scene )))

(defmacro add-scene-entity ( scene data )
  `(setf ( gethash ( tamias.entities:t-object-symbol ,data )
		   ( scene-objects ,scene ))
	 ,data ))




(defun add-entity-to-scene ( entity &optional ( scene current-scene ))
  (add-scene-entity scene
		    entity )
  (push ( entity-symbol entity ) ( scene-entities-ids scene )))

(defun remove-entity-from-scene ( entity &optional scene )
  (setf ( elt ( layer-objects-ids ( get-layer scene ( entity-layer-id entity )))
	      ( position ( tamias.entities:t-object-symbol entity )
			 ( layer-objects-ids ( get-layer scene ( entity-layer-id entity )))))
	nil
	( get-scene-entity entity scene )
	nil ))

