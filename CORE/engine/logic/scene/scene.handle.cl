(defun handle-scene ( &optional scene )
  (loop :for l-id :in ( scene-layers-ids scene )
	:do (let (( layer ( gethash l-id ( scene-layers scene ))))
	     ( handle-layer layer ))))

