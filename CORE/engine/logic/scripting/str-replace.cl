(defun replace-string (old-seq new-seq text)
  (let* ((start-char (aref old-seq 0))
	 (ret-text (copy-seq text))
	 (old-seq-length (length old-seq))
	 (end-char (aref old-seq (1- old-seq-length)))
	 (cur-pos 0)
	 (ret-str ""))
    ;;needs debugging
    ;;(replace-string "123" "abc" "123 xyz") works
    ;;but
    ;;(replace-string "123" "abc" "1123 xyz") doesn't
    ;;for the scripting engine, this shouldn't be an issue, but it does need to
    ;;be fixed in the inevitable event it actually fucking does
    (loop while (< cur-pos (1- (length text)))
	  do (if (and (find start-char ret-text)
		      (find end-char ret-text))
		 (let (found
		       (cur-pos-old 0))
		   (loop :for txt-pos :from (position start-char text :start cur-pos)
			   :to (+ old-seq-length
				  (1- (position start-char text :start cur-pos)))
			 do (if (eql (aref old-seq cur-pos-old)
				     (aref text txt-pos))
				(setf found t)
				(return (progn (setf found nil) (incf cur-pos-old))))
			    (incf cur-pos-old))
		   (if found 
		       (setf ret-str
			     (concatenate 'string ret-str
					  (subseq text cur-pos
						  (position start-char text :start cur-pos))
					  new-seq)
			     cur-pos (1+ (position end-char text :start (+ cur-pos (1- old-seq-length)))))
		       (progn (setf ret-str
				    (concatenate 'string ret-str
						 (subseq text cur-pos
							 (+ cur-pos cur-pos-old))))
			      (incf cur-pos cur-pos-old)))
		   (setf ret-text (subseq text cur-pos))
;;		   (print ret-text)
;;		   (print ret-str)
		   )
		 (return (setf ret-str
			       (concatenate 'string ret-str
					    ret-text)))))
    ret-str))

(defun search.char (search-for-this-seq in-this-seq)
  (search search-for-this-seq in-this-seq :test #'char-equal))

(defun string.replace-with.symbol (orig-symbol rep-symbol text)
  (let ((orig-string (write-to-string orig-symbol))
	(rep-string (write-to-string rep-symbol))
	(text-copy (copy-seq text))
	strs
	done)
    (loop while (not done)
	  do (let ((pos (search.char orig-string text-copy)))
	       (if pos
		   (progn (aux:push-to-end (subseq text-copy 0 pos) strs)
			  (setf text-copy (subseq text-copy (+ pos (length orig-string)))))
		   (progn (aux:push-to-end (subseq text-copy 0) strs)
			  (setf done t)))))
    (setf done "")
    (if strs
	(loop while strs
	      do (let ((con-str nil))
		   (setf con-str (pop strs))
		   (if strs
		       (setf con-str (concatenate 'string con-str rep-string)))
		   (setf done (concatenate 'string done con-str)))))
    done))
