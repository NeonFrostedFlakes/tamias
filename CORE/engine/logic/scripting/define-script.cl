#|
So, how the hell am I gonna do this?

I think the best way right now is to
|#


(defpackage :tamias.scripts
  (:use :cl)
  (:export entity.define-script))
(in-package :tamias.scripts)

(defvar script-symbol-table
  (list (list :render (concatenate 'string " entity.render" '(#\newline)
				   "(entity"  '(#\newline)
				   " (entity-type (eql "))
	(list :logic (concatenate 'string " entity.logic" '(#\newline)
				  "(entity" '(#\newline)
				  " (entity-type (eql "))))

(defmacro script-type-string (key)
  `(cadr (assoc ,key script-symbol-table)))

;;(define-symbol-macro entity.x (tamias.entities:entity-x entity))
;;(define-symbol-macro entity.y (tamias.entities:entity-y entity))

(defmacro entity.define-script (script-type-keyword entity-type-keyword &body body)
  `(let ((eval-str ""))
     (if (assoc ,script-type-keyword script-symbol-table)
	 (setf eval-str (concatenate 'string "(defmethod" (script-type-string ,script-type-keyword) 
				     (write-to-string ,entity-type-keyword)
				     ")) )" '(#\newline)
				     (write-to-string ',@body)
				     ")"))
	 "SCRIPT TYPE NOT RECOGNIZED")))
