;;load up the str-replacer stuff :) I didn't work on it for nothing (I think it's cross-implementation too)

#|
(defun interpret-script-file (file script-type)
  (let ((script-string ""))
    (with-open-file (f-stream file)
      (loop for obj = (read f-stream nil nil)
	    while obj
	    do (print (obj-script-string obj))
#|	       (if (listp obj)
		   (if (eq (car obj) 'lisp)
		       (print (write-to-string (cdr obj)))
		       (print obj))
		   (print (type-of obj)))|#
       ))
    script-string))
|#

(defun encapsulte-code? (function code)
  (concatenate 'string "(" function code ")"))

(defun obj-script-string (obj)
  (let ((ret-str ""))
    (if (listp obj)
	(if (or (= (car obj) 'lisp)
		(= (car obj) 'cl))
	    (setf ret-str (loop for child in obj
				collect child))
	    (setf ret-str (loop for child in obj
				collect (obj-script-string child))))
	(let ((obj-string (get-script-string obj)))
	  (if obj-string
	      obj-string
	      obj)))))

(defun load-tamias-script (t-script)
  (let ((ret-str ""))
    (setf ret-str (uiop:read-file-string t-script))))

;;I will need a sample tsf file (tamias script file)
;;No I won't. tsf doesn't exist ()

(defun script-interpret (read-stream)
  (if (stringp read-stream)
      'is-string
      'is-stream))

(defun interpret-script-file (t-script)
  (let* ((script-str (load-tamias-script t-script))
	 (script-stream (make-string-input-stream script-str)))
    (loop while (> (length script-str) 1)
	  do (if (eql (aref script-str 0) #\()
		 (let ((script-read (read script-stream)))
		   (script-interpret script-read)
		   (setf script-str (subseq script-str (length (write-to-string script-read)))))
		 (let ((script-read (read-line script-stream)))
		   (script-interpret script-read)
		   (setf script-str (subseq script-str (length script-read))))))))
;;up here is code that can do both read-line and read a list i.e. (lisp blah blah blah)



