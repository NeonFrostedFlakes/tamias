Scripts that are used to 'define' tamias objects or entities

Some declaration will be needed at the top of the file

Examples:
entity.define
ui.define
t-object.define
item.define

However, I may change the extension to what is being defined (the base type) e.g.
minotaur.entity check-box.ui wall.t-object armor.item

While this would create some problems with what could be added, it would be clear what structure is being used as the base.

This might be able to be extended by using something like (:use [extension])

More complex definition files will be available as long as you confirm their usage within your project


Optimally, you could do something like minotaur.entity, then minotaur-guard.minotaur, where minotaur is inherited (rather than entity) by the minotaur-guard defstruct
e.g. (defstruct (minotuar (:include entity))) (defstruct (minotaur-guard (:include minotaur)))

You could also use something like attack.remove to remove an attack from an entity that is a sub-class of a defined entity
