(tamias:console.add-message "Use C-` [Control-backquote] to exit the program")
(tamias:console.add-message "` [backquote] will close the console")

(defun render-messages ()
  (render:box 0 0 tamias:screen-width (floor (/ tamias:screen-height 2)) tamias.colors:+black+)
  (let ((message-counter 0))
    (loop for message in tamias:messages
       do (if (< (tamias:message-timer message) tamias:message-timer)
	      (let ((text (text-chunker (tamias:message-text message) (round (/ tamias:screen-width 16)))))
		(incf (tamias:message-timer message))
		(loop :for txt :in text
		      :do (render:text txt 0 (* message-counter 16) :color tamias:messages-color)
		      	  (incf message-counter)))
	      (setf tamias:messages (remove message tamias:messages)))
	 (if (> message-counter (floor (/ tamias:screen-height 32)))
	     (return t)))))

(defun ui.render-elements ()
  )

(defun render-state ()
  (tamias:render-state tamias:state (tamias:state-sub-state-stack tamias:state))
  (ui.render-elements)
  (if tamias:console.show
      (render-messages)))

(defvar +transition-box-alpha+ 0)
(defvar +transition-state+ 'to)
(defun process-changing-state ()
  (if tamias:changing-state
      (if (and (state-transition (eval tamias:state))
	       (state-transition (eval tamias:changing-state)))
	  (progn (render:box 0
			     0
			     tamias:screen-width
			     tamias:screen-height
			     (list 0 0 0 +transition-box-alpha+))
		 (if (eq +transition-state+ 'to)
		     (incf +transition-box-alpha+ 12)
		     (decf +transition-box-alpha+ 12))
		 (if (< +transition-box-alpha+ 0)
		     (setf +transition-box-alpha+ 0))
		 (if (> +transition-box-alpha+ 255)
		     (setf +transition-box-alpha+ 255))
		 (if (eq +transition-box-alpha+ 255)
		     (progn (setf +transition-state+ 'from
				  tamias:state tamias:changing-state)
			    #|(if (eq state 'level)
				(start-game))
			    commenting this out shouldn't affect anything
			    but, every change breaks something|#
			    ))
		 (if (eq +transition-box-alpha+ 0)
		     (setf tamias:changing-state nil
			   +transition-state+ 'to)))
	  (setf tamias:state tamias:changing-state
		tamias:changing-state nil))))
