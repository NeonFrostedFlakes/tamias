(defstruct tamias-player
  name
  (input (make-hash-table))
  (number 1))
(defvar tamias-players nil)

(defmacro create-tamias-player (&optional (name "tamias-player-n") (number 1))
  `(let ((player (make-tamias-player :name ,name :number ,number)))
     (setf (gethash :down (tamias-player-input player)) (make-hash-table))
     (setf (gethash :up (tamias-player-input player)) (make-hash-table))

     (setf (gethash 1001 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1010 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1011 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1100 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1101 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1110 (gethash :up (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1111 (gethash :up (tamias-player-input player))) (make-hash-table))

     (setf (gethash 1001 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1010 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1011 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1100 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1101 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1110 (gethash :down (tamias-player-input player))) (make-hash-table))
     (setf (gethash 1111 (gethash :down (tamias-player-input player))) (make-hash-table))
     (push player tamias-players)))

(defun get-player-key (key (key-state &key ctrl alt shift))
  (let ((mask 1000))
    (if shift
	(incf mask 1))
    (if ctrl
	(incf mask 10))
    (if alt
	(incf mask 100))
    (if (eq mask 1000)
	(eval (gethash key (gethash key-state (tamias-player-input player))))
	(eval (gethash key (gethash mask (gethash key-state (tamias-player-input player))))))))

    

(defmacro add-player-key (player key (key-state &key ctrl alt shift) &body body)
  `(let ((mask 1000))
    (if shift
	(incf mask 1))
    (if ctrl
	(incf mask 10))
    (if alt
	(incf mask 100))
    (if (eq mask 1000)
	(setf (gethash key (gethash key-state (tamias-player-input player))) `,',@body)
	(setf (gethash key (gethash mask (gethash key-state (tamias-player-input player)))) `,',@body))))
