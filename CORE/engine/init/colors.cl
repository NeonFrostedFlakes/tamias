(defpackage tamias.colors
  (:use :cl)
  (:export def-color
	   def-hex-color
	   +black+
	   +white+
	   +pastel-grey+
	   +dark-pastel-grey+
	   +pastel-dark-grey+

	   +pastel-gray+                   
	   +dark-pastel-gray+                
	   +pastel-dark-gray+                

	   +cream+                   
	   +red+               
	   +dark-red+               
	   +green+               
	   +blue+               
	   +yellow+                   
	   +pastel-red+                   
	   +dark-pastel-red+                 
	   +pastel-green+                   
	   +pastel-blue+                   
	   +pastel-yellow+                   
	   +pastel-pink+                   
	   +dark-pastel-pink+

	   +aqua+                   
	   +purple-jade+                  
	   +cobalt+                 
	   +yellow-zinc+                 
	   +chalk-white+                   
	   +brown+                
	   +natural-green+                  
	   +dark-natural-green+                
	   +navy-blue+                
	   +steel-blue+                  
	   +dark-steel-blue+                
	   +orange+
	   offset))
(in-package :tamias.colors)

(defun offset (color &optional (offset-by? 20))
  (let ((ret-val (vector (+ (elt color 0) offset-by?)
			 (+ (elt color 1) offset-by?)
			 (+ (elt color 2) offset-by?)
			 255)))
    (if (> (length color) 2)
	(setf (elt ret-val 3) (elt color 3)))
    (loop for ind below 4
	  do (if (> (elt ret-val ind) 255)
		 (setf (elt ret-val ind) 255)
		 (if (< (elt ret-val ind) 0)
		     (setf (elt ret-val ind) 0))))
    ret-val))

#|    (loop for n below 3
	  do (if (< (elt ret-val n) 0)
		 (setf (elt ret-val n) 0)
		 (if (> (elt ret-val n) 255)
		     (setf (elt ret-val n) 255))))))
|#
(defmacro def-color (color-name r g b)
  `(defparameter ,color-name (list ,r ,g ,b 255)))
(defmacro def-hex-color (color-name hex-code)
  `(defparameter ,color-name (tamias.string:parse-hex-color ,hex-code :return-type 'list)))

(def-hex-color +pastel-purple+ "C8A2C8")
(export '+pastel-purple+)
(def-hex-color +dark-magenta+ "8B008B")
(export '+dark-magenta+)

(defparameter +black+ '(0 0 0 255))
(defparameter +white+ '(255 255 255 255))
(defparameter +pastel-grey+ '(207 207 196 255))
(defparameter +dark-pastel-grey+ '(52 52 40 255))
(defparameter +pastel-dark-grey+ '(52 52 40 255))

(defparameter +pastel-gray+ '(207 207 196 255))
(defparameter +dark-pastel-gray+ '(52 52 40 255))
(defparameter +pastel-dark-gray+ '(52 52 40 255))

(defparameter +cream+ '(255 253 208 255))
(defparameter +red+ '(255 0 0 255))
(defparameter +dark-red+ '(127 0 0 255))
(defparameter +green+ '(0 255 0 255))
(defparameter +blue+ '(0 0 255 255))
(defparameter +yellow+ '(252 241 125 255))
(defparameter +pastel-red+ '(253 139 139 255))
(defparameter +dark-pastel-red+ '(160 69 69 255))
(defparameter +pastel-green+ '(180 242 156 255))
(defparameter +pastel-blue+ '(167 236 251 255))
(defparameter +pastel-yellow+ '(255 255 186 255))
(defparameter +pastel-pink+ '(255 209 220 255))
(defparameter +dark-pastel-pink+ (tamias.string:parse-hex-color "e5b3b7" :return-type 'list))

(defparameter +aqua+ '(160 217 208 255))
(defparameter +purple-jade+ '(225 61 147 255))
(defparameter +cobalt+ '(5 128 198 255))
(defparameter +yellow-zinc+ '(227 210 8 255))
(defparameter +chalk-white+ '(245 245 245 255))
(defparameter +brown+ '(93 67 45 255))
(defparameter +natural-green+ '(93 161 113 255))
(defparameter +dark-natural-green+ '(46 80 56 255))
(defparameter +navy-blue+ '(45 71 93 255))
(defparameter +steel-blue+ '(70 130 180 255))
(defparameter +dark-steel-blue+ '(35 65 90 255))
(defparameter +orange+ '(255 165 0 255))
