(defpackage :tamias.aux
  (:use cl)
  (:nicknames :t.aux :aux)
  (:export push-init
	   push-quit
	   add-init
	   add-quit
	   CATCH-ONCE
	   catch-values
	   catch-value
	   push-to-end
	   read-string
	   eval-string
	   length>
	   define-enum
	   cycle-selection
	   to-keyword))
(in-package :tamias.aux)

(defmacro push-init (&body func-sym-or-body)
  `(push ',@func-sym-or-body cl-user:init-functions))
(defmacro push-quit (&body func-sym-or-body)
  `(push ',@func-sym-or-body cl-user:quit-functions))
(defmacro add-init (&body func-sym-or-body) ;;add-init for when the window is open and mixer is loaded
  `(push ',@func-sym-or-body cl-user:init-functions)) ;;i.e. anything that requires allocating memory with the
(defmacro add-quit (&body func-sym-or-body) ;; backend stuff.
  `(push ',@func-sym-or-body cl-user:quit-functions)) ;;push/add-quit for freeing memory or resources that have to be reset


(defun to-keyword (obj)
  (read-from-string (concatenate 'string ":" (if (not (stringp obj))
						 (write-to-string obj)
						 obj))))

(defmacro length> (str num)
  `(> (length ,str) ,num))

(DEFVAR CATCH-ONCE NIL)
(DEFVAR CATCH-VALUES NIL)
(DEFUN CATCH-VALUE (VALUE)
  (IF (NOT CATCH-ONCE)
      (PUSH VALUE CATCH-VALUES))
  (SETF CATCH-ONCE T))
(DEFUN CATCH-VALUES (&rest VALUE)
  (PUSH VALUE CATCH-VALUES))

(defmacro push-to-end (obj lst)
  `(if ,lst
       (push ,obj (cdr (last ,lst)))
       (push ,obj ,lst)))
;;Adapted from
;;https://stackoverflow.com/questions/13359025/adding-to-the-end-of-list-in-lisp

(defmacro read-string (str)
  `(read (make-string-input-stream ,str)))
(defmacro eval-string (str)
  `(eval (read-string ,str)))


(defmacro define-enum (&rest consts)
  `(loop :for const :in ',consts
      :do (let ((const-name (car const)) (const-val (cadr const))) (print const-name)
	      (eval `(defparameter ,const-name ,const-val)))))

;;(tam-enum (top-left 0) (top-right 1) (center 2) (bottom-left 3) (bottom-right 4))

;;selector.cl

(defmacro cycle-selection (selection direction &key (min 0) max)
  `(let ((down :down)
	 (up :up)
	 (selection ,selection)
	 ;;selection ,selection because ,selection could be (ui-selector (gethash 1 (gethash :idle (gethash :gui-tester ui-managers)))), and evaluating that multiple times can cause performance issues.
	 (max ,max)
	 (min ,min))
     (if (or (eq ,direction 1)
	     (eq ,direction down))
	 (incf selection)
	 (if (or (eq ,direction -1)
		 (eq ,direction up))
	     (decf selection)))
     (if max
	 (if (> selection max)
	     (setf selection max)))
     (if (< selection min)
	 (setf selection min))
     (setf ,selection selection)))


(defmacro a<b<c (a n c)
  `(and (> ,n ,a)
	(< ,n ,c)))

(defmacro a<b<=c (a n c)
  `(and (> ,n ,a)
	(<= ,n ,c)))

(defmacro a<=b<=c (a n c)
  `(and (>= ,n ,a)
	(<= ,n ,c)))

(defmacro a>b>c (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))

(defmacro a>=b>=c (a n c)
  `(and (<= ,n ,a)
	(>= ,n ,c)))

(defmacro a>b>=c (a n c)
  `(and (< ,n ,a)
	(>= ,n ,c)))

(defmacro >b> (a n c)
  `(and (< ,n ,a)
	(> ,n ,c)))
	
(defmacro length> (sequence comparator)
  `(if (listp ,comparator)
       (> (length ,sequence) (length ,comparator))
       (> (length ,sequence) ,comparator)))

(defmacro length< (sequence comparator)
  `(if (listp ,comparator)
       (< (length ,sequence) (length ,comparator))
       (< (length ,sequence) ,comparator)))

(defmacro length= (sequence comparator)
  `(if (listp ,comparator)
       (= (length ,sequence) (length ,comparator))
       (= (length ,sequence) ,comparator)))

(export '(a<b<c
	  a<=b<=c
	  a>b>c
	  a>=b>=c
	  >a>
	  length>
	  length<
	  length=))
