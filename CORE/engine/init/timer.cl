(defpackage :timer
  (:use :cl)
  (:export timers
	   make
	   current
	   end
	   reset
	   reset-timer
	   end?
	   incrementor
	   increment
	   increment-timers
	   add
	   push-timer
	   remove-timer
	   timers-table))
(in-package :timer)
(defvar timers nil)
(defvar timers-table ( make-hash-table ))
(defvar timer-fps 60)
(defstruct timer
  current
  end
  incrementor
  reset )

(defun make ( &key ( current 0 ) ( end 1 ) ( incrementor 1 ) reset in-seconds?)
  (when in-seconds?
    (setf end ( round ( * end tamias:fps ))))
  (when ( < incrementor 0 )
    (when ( > end 0 )
      (setf end ( * end -1 ))))
  (make-timer :current current :end end :incrementor incrementor :reset reset ))

(defmacro current ( timer )
  `(timer-current ,timer ))
(defmacro end ( timer )
  `(timer-end ,timer ))
(defmacro incrementor ( timer )
  `(timer-incrementor  ,timer ))
(defmacro reset ( timer )
  `(timer-reset ,timer ))
(defun reset-timer ( timer )
  (setf ( timer-current timer ) 0 ))
(defmacro increment ( timer )
  `(incf ( timer-current ,timer ) ( timer-incrementor ,timer )))

(defun remove-timer ( timer )
  (setf timer:timers ( remove timer timer:timers )))

(defmacro pkg-reset-timer ( timer &optional timers? )
  `(progn (when ( timer-reset ,timer )
	    ( setf (timer-current ,timer ) 0 ))
	  (when ,timers?
	    ( remove-timer ,timer ))
	  t ))

(defun end? ( timer &optional timers? )
  (if ( >= ( timer-incrementor timer ) 0 )
      (when ( >= ( timer-current timer ) ( timer-end timer ))
	( pkg-reset-timer timer timers? ))
      (when ( <= ( timer-current timer ) ( timer-end timer ))
	( pkg-reset-timer timer timers? ))))
   

(defun push-timer ( timer )
  (push timer timer:timers ))
(defun add ( timer )
  (push timer timer:timers ))

(defun increment-timers ()
  (loop for timer in timers
     do (if ( >= ( timer-incrementor timer ) 0 )
	    (when ( >= ( timer-current timer ) ( timer-end timer ))
	      ( remove-timer timer ))
	    (when ( <= ( timer-current timer ) ( timer-end timer ))
	      ( remove-timer timer )))
	(incf ( timer-current timer ) ( timer-incrementor timer ))))

"A quick thing about timers: they are generalized. So, you can use a timer for a literal timer (i.e. counting down from 60 to 0) 
or you can use it in combination with a GUI (i.e. counting down from 1000 gold to 25 gold)"
