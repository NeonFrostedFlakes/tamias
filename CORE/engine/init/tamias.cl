(defvar exit-tamias? nil)
(export 'exit-tamias?)

(defmacro remove-nil (lst)
  `(setf ,lst (remove nil ,lst)))

(defun sharp-h (stream subchar arg) (declare (ignore stream subchar arg)) (make-hash-table))
(set-dispatch-macro-character #\# #\h #'sharp-h)
(defun sharp-f (stream subchar arg)
  (declare (ignore subchar arg))
  (let ((var (read stream nil nil)))
    (if var
	(eval `(defvar ,var 0.0))
	0.0)))
(set-dispatch-macro-character #\# #\f #'sharp-f)

#|
(defun sharp-g (stream subchar arg) 
	   (declare (ignore subchar arg))
	   `(defvar ,(read stream) ,(read stream)))
This is for documentation purposes :)
|#


(defun sharp-v (stream subchar arg) 
	   (declare (ignore subchar arg))
	   (let* ((ln (read-line stream))
		  (obj-1 (read-string ln))
		  (obj-2 (if (find #\space ln)
			     (read-string (subseq ln (position #\space ln)))
			     nil)))
	     `(defvar ,obj-1 ,obj-2)))
#|
(defun sharp-v (stream subchar arg) 
	   (declare (ignore subchar arg))
  `(defvar ,(read stream) ,(read stream nil nil)))
This version will take the next object found and use it as the value for your variable
so would result in my-var evaluating to "fuck"
  |
  V
#vmy-var
(print "fuck")
    
|#
(set-dispatch-macro-character #\# #\v #'sharp-v)
;;#vVariable-name-with-unlimited-hyphens-:] and-optional-value_otherwise-is-nil


(defconstant +idle+ :idle)
(export '+idle+)

(defvar init-functions nil)
(defvar quit-functions nil)

(defmacro push-init (&body func-sym-or-body)
  `(push ',@func-sym-or-body init-functions))
(defmacro push-quit (&body func-sym-or-body)
  `(push ',@func-sym-or-body quit-functions))
(defmacro add-init (&body func-sym-or-body) ;;add-init for when the window is open and mixer is loaded
  `(push ',@func-sym-or-body init-functions)) ;;i.e. anything that requires allocating memory with the
(defmacro add-quit (&body func-sym-or-body) ;; backend stuff.
  `(push ',@func-sym-or-body quit-functions)) ;;push/add-quit for freeing memory or resources that have to be reset
(export '(push-init push-quit add-init add-quit))
(export '(init-functions quit-functions))

(defmacro string! (object)
  `(write-to-string ,object))

(defpackage :tamias
  (:use :cl)
  (:nicknames :states
	      :engine)
  (:export quad-tree
	   started?
	   main-release
	   set-window-title
	   game-title
	   *mouse-x*
	   *mouse-y*
	   *mouse-velocity-x*
	   *mouse-velocity-y*
	   *cursor-size*
	   track-volume
	   max-volume
	   current-track
	   new-track
	   default-volume-state
	   key-pressed
	   screen-surface
	   default-window
	   window-width
	   window-height
	   screen-width
	   screen-height
	   state
	   sub-state
	   changing-state
	   renderer
	   accumulator
	   selection
	   font
	   ttf-font
	   ttf-font-size
	   ttf-font-path
	   max-characters
	   text-buffers
	   cell-accumulator
	   volume-state
	   objects
	   render-clear-color
	   resolution
	   update-time
	   fps
	   resolution-list
	   messages
	   messages-counter
	   messages-color
	   enable-ttf-font
	   using-gui
	   console.add-message
	   console.show
	   console.hide
	   console.toggle
	   set-window-size
	   message-timer
	   message-text
	   message-buffer
	   load-image
	   make-rect
	   set-default-window-size
	   change-resolution
	   set-resolution
	   main
	   create-exec
	   load-font
	   free-image
	   set-scale-factor
	   scale-factor
	   set-target-resolution
	   target-resolution
	   target-resolution-changed))

(in-package :tamias)


(defun load-font (&key font-type font-path)
  (declare (ignore font-type font-path)))

(defvar started? nil)

(defvar quad-tree nil)

(defun main ()
  )

(defun main-release ()
  )

(defvar default-window nil)

(defun set-window-size (window width height)
  (declare (ignore window width height)))
(defun load-image (file-path)
  (declare (ignore file-path))) ;;loader function for backends
(defmacro free-image (img)
  (declare (ignore file-path))) ;;freer for backend
(defun make-rect (x y width height)
  (declare (ignore x y width height)))
(defun set-default-window-size (&optional width height)
  (set-window-size default-window width height))


(defvar *mouse-x* 0)
(defvar *mouse-y* 0)
(defvar *mouse-velocity-x* 0)
(defvar *mouse-velocity-y* 0)
(defparameter *cursor-size* 5)

(defvar track-volume 125)
(defvar max-volume 125)
(defvar current-track nil)
(defvar new-track nil) ;;needs to be a symbol
(defvar default-volume-state :increasing)

(defvar key-pressed nil)
(defvar screen-surface nil)
(defvar window-width 0)
(defvar window-height 0)
(defvar game-title "WINDOW TITLE TAMIAS - CHANGE BY USING (TAMIAS:SET-WINDOW-TITLE \"YOUR-TITLE\")")
(defun set-window-title (title)
  )

(defvar state nil) ;;specifically an empty state, does nothing...when it's tamias-default-state that is...
(defvar sub-state 'top)
(defvar changing-state nil) ;;setf changing-state to 'state-to-change-to (i.e. main-menu)
(defvar renderer nil)
(defvar accumulator 0)
(defvar selection 0)

(defvar font nil)
(defvar ttf-font nil)
(defvar ttf-font-size 12)
(defvar ttf-font-path "fonts/linux_libertine/LinLibertine_R.ttf")
(setf ttf-font-path "fonts/Vera.ttf")

(defvar max-characters '(40 40))
(defvar text-buffers nil)
(defvar cell-accumulator 0)
(defvar volume-state nil)
(defvar objects nil)
(defvar render-clear-color '(0 0 0))
(defvar resolution 1)
(defvar update-time 5)
(defvar fps 60)
(defvar resolution-list '(#(800 600)
			  #(960 720)
			  #(1024 768)
			  #(1280 720)
			  #(1360 760)
			  #(1280 1024)
			  #(1440 900)
			  #(1680 1050)
			  #(1280 960)
			  #(1440 1080)
			  #(1920 1080)))
(setf window-width (elt (nth resolution resolution-list) 0))
(setf window-height (elt (nth resolution resolution-list) 1))

(defun set-window-dim-by-res-list ()
  (setf window-width (elt (nth resolution resolution-list) 0)
	window-height (elt (nth resolution resolution-list) 1)))

(defun change-resolution (by-method width-or-index &optional (height 1))
  (declare (ignore by-method width-or-index height)))

(defun set-resolution (indice &optional (window default-window))
  (setf resolution indice)
  (set-window-dim-by-res-list)
  (set-window-size window window-width window-height))

(defun enable-ttf-font ()
  (if (not ttf-font)
      (setf ttf-font t)
      (tamias:console.add-message "TTF font is already enabled")))

(defstruct message
  (timer 0)
  text
  buffer)
(defvar message-timer 240)

(defvar messages (list (make-message :text "Console started")))
(defvar messages-counter 0)
(defvar messages-color '(255 255 255 255))

(defvar using-gui t)

(defun create-exec (&key linking (name "Game") (system :tamias))
  )

(defun fps (target)
  (setf fps target
	update-time (floor (/ 1000 (* 2 target)))))

(defvar screen-width 640)
(defvar screen-height 480)

(defvar scale-factor (vector window-width window-height))
(defvar target-resolution scale-factor)
(defvar target-resolution-changed nil)

(defun set-scale-factor (&optional width height)
  (setf scale-factor (vector width height)
	target-resolution-changed nil))

(defun set-target-resolution (&optional (width window-width) (height window-height))
  (setf target-resolution (vector width height)
	target-resolution-changed t))
