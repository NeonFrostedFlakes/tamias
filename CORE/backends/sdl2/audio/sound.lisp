(defstruct sound
  path
  chunk
  channel)
(defvar current-channel -1)
(defvar sfx-volume 128)
(defvar *tamias-sounds* (make-hash-table))
(defvar *tamias-sounds-list* nil)

(defun init-sound (sound)
  (setf (sound-chunk (gethash sound *tamias-sounds*))
	(sdl2-mixer:load-wav (sound-path (gethash sound *tamias-sounds*)))))

(defun init-sounds ()
  (loop for sound in *tamias-sounds-list*
	do (init-sound sound)))

(defmacro define-sound (var path)
  `(progn (setf (gethash (aux:to-keyword ',var) *tamias-sounds*) (make-sound :path ,path :channel (incf current-channel)))
	  (push (aux:to-keyword ',var) *tamias-sounds-list*)))

(setf sfx-volume 96)

(defun play-sound (sound)
  (let ((sample (gethash (aux:to-keyword sound) *tamias-sounds*)))
    (sdl2-mixer:volume (sound-channel sample) sfx-volume)
    (sdl2-mixer:play-channel (sound-channel sample) (sound-chunk sample) 0)))
#|
  (let ((sound (sdl2-mixer:load-wav sound)))
;;;;    (setf sample-track sound)
    (sdl2-mixer:play-channel channel sound 0)
;;;;    (free-sound sound)
    )
  )
|#

(defun free-sound (sound)
  (sdl2-mixer:halt-channel 0)
  (sdl2-mixer:free-chunk (sound-chunk sound))
  (setf (sound-chunk sound) nil))

(defun free-sounds ()
  (sdl2-mixer:halt-channel 0)
  (loop for sound in *tamias-sounds-list*
     do (sdl2-mixer:free-chunk (sound-chunk (gethash sound *tamias-sounds*)))))
