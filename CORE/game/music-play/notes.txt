This will be a simple file browser that can play OGG files to begin with.

+-----------------------------------+
|File|View|...			    |
|-----------------------------------|
|Shortcuts| Files		    |
|	  | And			    |
|	  | Folders		    |
|	  |			    |
|	  |			    |
|	  |			    |
|	  |			    |
|	  |			    |
|	  |			    |
|	  |			    |
|	  |			    |
|-----------------------------------|
|Prev|Play|stop|next | --:--/--:--  |
|-----------------------------------|
|O=========================OSeek_bar|
+-----------------------------------+

Redo CL-FM with current Tamias engine

Only show folders and .ogg files
On enter, check if folder or file
=Folder? Ignore track, go to new directory
-File? Check current-track
--if current, stop and unload old track, load and play new track
--if not, (as in current-track -> nil) then load and play track

-Focus on /making/ the player, improvements, like play/stop buttons (and preset palettes) can come later
-This is supposed to be a real world example of useful software built on top of tamias
