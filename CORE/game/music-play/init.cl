(define-state cl-fm)
(set-state cl-fm)

(state.init-ui 'cl-fm 'top)

(defvar icons '((:left-arrow "<")
		(:right-arrow ">")
		(:up-arrow "^")
		(:plus "+")))

(defvar fm-cwd (sb-posix:getcwd))
(defvar fm-cwd-files (list-directory fm-cwd :to-var t))
(defvar fm-cwd-files-buffer nil)
(defvar cl-fm-menus nil)
(defvar cur-file-num 0)
(defvar files-pane-offset 0)

(change-palette pink-palette)

(defun set-fm-files ()
  (setf fm-cwd (sb-posix:getcwd)
	fm-cwd-files (list-directory fm-cwd :to-var t)
	fm-cwd-files-buffer (create-text-buffer fm-cwd-files :width 1920
						:height (* (count #\newline fm-cwd-files) (cadr character-size))
						:to-texture t :string-case 'text)))

(defvar back-directories "/home")
(defvar forward-directories nil)



(defun enter-directory (directory)
  (push fm-cwd back-directories)
  (setf fm-cwd directory)
  (sb-posix:chdir fm-cwd)
  )

(defun up-directory ()
  (push fm-cwd back-directories)
  (setf fm-cwd (subseq fm-cwd 0 (position #\/ fm-cwd :from-end t)))
  (enter-directory))

(defun back-directory ()
  (setf fm-cwd (pop back-directories))
  )

(defun fm.load-track (file)
  (tamias-music:load-track (concatenate 'string fm-cwd "/" file))
  )
  










"USE BELOW AS REFERENCE CODE"


(state.init-ui 'stock-simulator 'top)
(def-render ('stock-simulator 'top)
    (render.ui-elements stock-simulator)
  (render.cursor))


(add-state-ui-element 'stock-simulator 'top
		      (make-ui-label :x 16 :x-equation 16 :y 16 :y-equation 16
				     :width (* 5 (car tamias.string:character-size)) :width-equation (* 5 (car tamias.string:character-size))
				     :height (+ (cadr tamias.string:character-size) 2) :height-equation (+ (cadr tamias.string:character-size) 2)
				     :label '(stock-ticker (nth 0 stocks))))















(defun render-file-manager ()
  (fm-screen)
  (if (not (string-equal (sb-posix:getcwd) fm-cwd))
      (set-fm-files))
  (let ((icons-string (combine-strings (cadr (assoc :left-arrow icons)) " " (cadr (assoc :right-arrow icons)) " " (cadr (assoc :up-arrow icons)) " " (cadr (assoc :plus icons)))))
    (render-string icons-string (menu-x fm-navigation) (menu-y fm-navigation)))
  (render-string fm-cwd (menu-x fm-working-directory) (menu-y fm-working-directory))
  (render-box (menu-x files-pane) (+ (menu-y  files-pane)  (- (* 16  cur-file-num) files-pane-offset))
	      (menu-width files-pane) 18 :color (nth 1 palette))
  (if fm-cwd-files-buffer
      (let ((dest-height (if (> (menu-height files-pane) (sdl2:texture-height fm-cwd-files-buffer))
			     (sdl2:texture-height fm-cwd-files-buffer)
			     (menu-height files-pane))))
	(tex-blit fm-cwd-files-buffer
		  :src (create-rectangle (list 0 files-pane-offset
					       (menu-width files-pane) (menu-height files-pane)))
		  :dest (create-rectangle (list (menu-x files-pane) (menu-y files-pane)
						(menu-width files-pane) dest-height))
		  :color font-palette))
      (set-fm-files)))
(add-to-state-render render-file-manager cl-fm)

#|(defmacro add-key (key state key-state &rest function-name)
  `(setf (gethash ,key (gethash ,key-state (state-keys ,state)))
	 `,(quote ,function-name)))
|#
(add-key :scancode-down cl-fm :down (if (< cur-file-num (1- (+ (round (/ (menu-height files-pane) 16)) (round (/ files-pane-offset 16)))))
					(if (< cur-file-num (1- (count #\newline fm-cwd-files)))
					    (incf cur-file-num 1))
					;;increase offset
					(if (< cur-file-num (count #\newline fm-cwd-files))
					    (progn (incf cur-file-num 1)
						   (incf files-pane-offset 16)))
					))
(add-key :scancode-up cl-fm :down (if (> cur-file-num 0)
				      (decf cur-file-num 1))
	 (if (eq cur-file-num (1- (/ files-pane-offset 16)));;(+ (round (/ (menu-height files-pane) 16)) (round (/ files-pane-offset 16))))
	     (decf files-pane-offset 16)))
				      
